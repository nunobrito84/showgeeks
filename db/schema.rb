# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150731204841) do

  create_table "artist_crawler_reports", force: :cascade do |t|
    t.integer  "artist_id",       limit: 4
    t.boolean  "automatic"
    t.time     "duration"
    t.integer  "success_count",   limit: 4,        default: 0
    t.integer  "edit_count",      limit: 4,        default: 0
    t.integer  "cancelled_count", limit: 4,        default: 0
    t.integer  "fail_count",      limit: 4,        default: 0
    t.integer  "skip_count",      limit: 4,        default: 0
    t.text     "log",             limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "artists", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "seller_id",          limit: 4
    t.boolean  "visible",                        default: true
    t.integer  "subcategory_id",     limit: 4
    t.string   "name",               limit: 255
    t.string   "address",            limit: 255
    t.string   "country_alpha2",     limit: 255
    t.string   "phone",              limit: 255
    t.string   "website",            limit: 255
    t.string   "external_icon",      limit: 255
    t.string   "icon_file_name",     limit: 255
    t.string   "icon_content_type",  limit: 255
    t.integer  "icon_file_size",     limit: 4
    t.datetime "icon_updated_at"
    t.string   "external_photo",     limit: 255
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
    t.boolean  "trusted"
    t.boolean  "enable_crawler"
    t.string   "crawler_feed_url",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",               limit: 255
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",       limit: 255
  end

  create_table "cities", force: :cascade do |t|
    t.string   "country_alpha2",  limit: 255
    t.string   "district_alpha2", limit: 255
    t.string   "name",            limit: 255
    t.string   "name_alt",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",            limit: 255
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "show_votes", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "show_id",    limit: 4
    t.integer  "vote_type",  limit: 4
    t.integer  "amount",     limit: 4
    t.string   "info",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shows", force: :cascade do |t|
    t.integer  "user_id",              limit: 4
    t.integer  "visible",              limit: 4,    default: 1
    t.string   "name",                 limit: 255
    t.boolean  "canceled",                          default: false
    t.integer  "subcategory_id",       limit: 4
    t.integer  "category_id",          limit: 4
    t.datetime "date"
    t.boolean  "is_date_only",                      default: false
    t.string   "country_alpha2",       limit: 255
    t.string   "district_alpha2",      limit: 255
    t.string   "city_id",              limit: 255
    t.string   "city_name",            limit: 255
    t.integer  "venue_id",             limit: 4
    t.string   "venue_name",           limit: 255
    t.string   "venue_website",        limit: 255
    t.string   "venue_icon",           limit: 255
    t.integer  "artist_id",            limit: 4
    t.string   "artist_icon",          limit: 255
    t.string   "url",                  limit: 255
    t.string   "description",          limit: 1024
    t.string   "photo_url",            limit: 255
    t.string   "price",                limit: 255
    t.integer  "trust",                limit: 4,    default: 0
    t.integer  "attending",            limit: 4,    default: 0
    t.string   "facebook_event_id",    limit: 255
    t.integer  "facebook_likes_count", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name",      limit: 255
    t.string   "photo_content_type",   limit: 255
    t.integer  "photo_file_size",      limit: 4
    t.datetime "photo_updated_at"
    t.string   "slug",                 limit: 255
  end

  create_table "subcategories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "category_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",        limit: 255
  end

  create_table "subcategory_votes", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "show_id",        limit: 4
    t.integer  "subcategory_id", limit: 4
    t.integer  "amount",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255, default: "",    null: false
    t.boolean  "admin",                              default: false, null: false
    t.boolean  "is_seller",                          default: false, null: false
    t.integer  "trust",                  limit: 4,   default: 1,     null: false
    t.string   "country_alpha2",         limit: 255
    t.string   "district_alpha2",        limit: 255
    t.string   "city_id",                limit: 255
    t.integer  "seller_id",              limit: 4
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "venue_crawler_reports", force: :cascade do |t|
    t.integer  "venue_id",        limit: 4
    t.boolean  "automatic"
    t.time     "duration"
    t.integer  "success_count",   limit: 4,        default: 0
    t.integer  "edit_count",      limit: 4,        default: 0
    t.integer  "cancelled_count", limit: 4,        default: 0
    t.integer  "fail_count",      limit: 4,        default: 0
    t.integer  "skip_count",      limit: 4,        default: 0
    t.text     "log",             limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "venues", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "seller_id",          limit: 4
    t.boolean  "visible",                        default: true
    t.string   "name",               limit: 255
    t.string   "address",            limit: 255
    t.string   "phone",              limit: 255
    t.integer  "city_id",            limit: 4
    t.string   "website",            limit: 255
    t.string   "external_icon",      limit: 255
    t.string   "icon_file_name",     limit: 255
    t.string   "icon_content_type",  limit: 255
    t.integer  "icon_file_size",     limit: 4
    t.datetime "icon_updated_at"
    t.string   "external_photo",     limit: 255
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
    t.float    "latitude",           limit: 24
    t.float    "longitude",          limit: 24
    t.boolean  "trusted",                        default: false
    t.boolean  "enable_crawler",                 default: false
    t.string   "crawler_feed_url",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",               limit: 255
  end

end
