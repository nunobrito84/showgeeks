require 'csv'

#Create admin
admin = User.find_by(email: 'nunobrito84@gmail.com')
if(admin.nil?)
  admin = User.create! :name => 'Admin',
                    :admin => true,
                    :email => 'nunobrito84@gmail.com',
                    :password => 'nambnamb',
                    :password_confirmation => 'nambnamb',
                    :avatar =>  File.new("#{Rails.root}/db/seed_data/admin_icon.png")
end

#Load categories
def self.load_categories_from_csv()
  file_name = "#{Rails.root}/db/seed_data/categories.csv"
  puts "creating categories from the file: #{file_name}"
  CSV.foreach(file_name, :headers => true, :col_sep => ';') do |row|
    puts "injecting #{row['name']}"
    Category.find_or_initialize_by(id: row['categorycode']).update_attributes(
        name: row['name']
    )
  end
end
load_categories_from_csv()

#Load subcategories
def self.load_subcategories_from_csv()
  file_name = "#{Rails.root}/db/seed_data/subcategories.csv"
  puts "creating subcategories from the file: #{file_name}"
  CSV.foreach(file_name, :headers => true, :col_sep => ';') do |row|
    puts "injecting #{row['name']}"
    Subcategory.find_or_initialize_by(id: row['id']).update_attributes(
        name: row['name'],
        category_id: row['categorycode']
    )
  end
end
load_subcategories_from_csv()

#Load cities
def self.load_cities_from_csv(country_alpha2)
  case country_alpha2
    when 'PT'
      file_name = "#{Rails.root}/db/seed_data/pt_cities.csv"
      puts "creating cities from the file: #{file_name}"
      CSV.foreach(file_name, :headers => true, :col_sep => ';') do |row|
        puts "injecting #{row['name']} (#{country_alpha2}; #{row['districtcode']})"
        City.find_or_initialize_by(country_alpha2: country_alpha2, district_alpha2: row['districtcode'], name: row['name']).update_attributes(
            country_alpha2: country_alpha2, district_alpha2: row['districtcode'], name: row['name'], name_alt: row['name_alt']
        )
      end
    when 'US'
      file_name = "#{Rails.root}/db/seed_data/us_cities.csv"
      puts "creating cities from the file: #{file_name}"
      CSV.foreach(file_name, :headers => true, :col_sep => ';') do |row|
        puts "injecting #{row['name']} (#{country_alpha2}; #{row['state_code']})"
        City.find_or_initialize_by(country_alpha2: country_alpha2, district_alpha2: row['state_code'], name: row['name']).update_attributes(
            country_alpha2: country_alpha2, district_alpha2: row['state_code'], name: row['name'], name_alt: row['name_alt']
        )
      end
  end
end
load_cities_from_csv('PT')
load_cities_from_csv('US')

#Create basic venues
=begin
p 'Creating Venues/Crawlers: '

p 'Yeaaaah.com'
v = Venue.find_or_create_by(name: 'Yeaaaah.com') do |venue|
  venue.public = false
  venue.user = admin
  venue.name = 'Yeaaaah.com'
  venue.address = 'online'
  venue.city_id = City.where(name: 'Lisboa', country_alpha2: 'PT').first.id
  venue.website = 'http://pt.yeaaaah.com/pt/agenda-de-concertos'
  venue.photo = File.new('db/seed_data/venues/PT/Online/Yeaaaah.com/logo.png')
  venue.icon =  File.new('db/seed_data/venues/PT/Online/Yeaaaah.com/icon.png')
  venue.trusted = false
  venue.enable_crawler = true
  venue.crawler_feed_url = 'http://pt.yeaaaah.com/pt/agenda-de-concertos'
end
#p v.errors.inspect

p 'Theatro Circo'
v = Venue.find_or_create_by(name: 'Theatro Circo') do |venue|
  venue.public = true
  venue.user = admin
  venue.name = 'Theatro Circo'
  venue.address = 'Av. da Liberdade, 697, 4710-251 Braga'
  venue.city_id = City.where(name: 'Braga', country_alpha2: 'PT').first.id
  venue.website = 'http://www.theatrocirco.com/'
  venue.photo = File.new('db/seed_data/venues/PT/Braga/theatro_circo/logo.jpg')
  venue.icon =  File.new('db/seed_data/venues/PT/Braga/theatro_circo/icon.png')
  venue.trusted = true
  venue.enable_crawler = true
  venue.crawler_feed_url = 'https://www.facebook.com/TheatroCircoFanPage/events'
end
=end

