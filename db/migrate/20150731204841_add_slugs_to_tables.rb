class AddSlugsToTables < ActiveRecord::Migration
  def change
    add_column :cities, :slug, :string
    add_column :venues, :slug, :string
    add_column :categories, :slug, :string
    add_column :subcategories, :slug, :string
    add_column :shows, :slug, :string
    add_column :artists, :slug, :string
  end
end
