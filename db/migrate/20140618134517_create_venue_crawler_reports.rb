class CreateVenueCrawlerReports < ActiveRecord::Migration
  def change
    create_table :venue_crawler_reports, :options => 'COLLATE=utf8_swedish_ci' do |t|
      t.integer :venue_id
      t.boolean :automatic
      t.time :duration

      t.integer :success_count,    default: 0
      t.integer :edit_count,       default: 0
      t.integer :cancelled_count,  default: 0
      t.integer :fail_count,       default: 0
      t.integer :skip_count,       default: 0

      t.text :log, limit: 16777215 #mediumtext

      t.timestamps
    end
  end
end
