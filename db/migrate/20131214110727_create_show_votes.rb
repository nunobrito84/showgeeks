class CreateShowVotes < ActiveRecord::Migration
  def change
    create_table :show_votes do |t|
      t.integer :user_id
      t.integer :show_id
      t.integer :vote_type
      t.integer :amount
      t.string  :info, default: nil

      t.timestamps
    end
  end
end
