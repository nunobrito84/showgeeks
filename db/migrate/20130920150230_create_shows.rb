class CreateShows < ActiveRecord::Migration
  def change
    #Use special charset for swedish and other languages special chars
    create_table :shows, :options => 'COLLATE=utf8_general_ci' do |t|
      t.integer :user_id

      t.integer :visible, default: Show::VISIBLE_YES #All shows are visible by default. It is a simple way to remove duplications

      t.string :name
      t.boolean :canceled, default: false
      t.integer :subcategory_id
      t.integer :category_id #denormalized for faster query
      t.datetime :date
      t.boolean :is_date_only, default: false

      #Location
      t.string :country_alpha2
      t.string :district_alpha2
      t.string :city_id
      t.string :city_name  #denormalized column for listing the city name fast

      #The venue details
      t.integer :venue_id
      t.string  :venue_name     #denormalized column for faster listing
      t.string  :venue_website  #denormalized column for faster listing
      t.string  :venue_icon     #denormalized column for faster listing

      #The artist details
      t.integer :artist_id
      t.string  :artist_icon     #denormalized column for faster listing

      #Show details
      t.string :url
      t.string :description, :limit => 1024 #allow 1024 chars max
      t.string :photo_url
      t.string :price

      t.integer :trust, default: 0  #The trust will be given from 0-100. The value 101 is for trusted sources, like 'theatro circo' web agenda
      t.integer :attending, default: 0  #The attending counts the people attending the show

      t.string :facebook_event_id     #This is the facebook event id
      t.integer :facebook_likes_count #This is the facebook likes count

      t.timestamps
    end

    #Add Paperclip attachment
    add_attachment :shows, :photo

  end
end
