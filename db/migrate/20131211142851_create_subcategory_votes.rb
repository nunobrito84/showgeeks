class CreateSubcategoryVotes < ActiveRecord::Migration
  def change
    create_table :subcategory_votes do |t|
      t.integer :user_id
      t.integer :show_id
      t.integer :subcategory_id
      t.integer :amount

      t.timestamps
    end
  end
end
