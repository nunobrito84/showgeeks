class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      #Custom data
      t.string  :name,              :null => false, :default => ''
      t.boolean :admin,             :null => false, :default => false
      t.boolean :is_seller,         :null => false, :default => false #If it is a seller or not
      t.integer :trust,             :null => false, :default => 1

      #This is used to hold the user
      t.string :country_alpha2
      t.string :district_alpha2
      t.string :city_id

      #This is used to get the client seller-customer relationship
      t.integer :seller_id  #foreign key for the same table

      ## Database authenticatable
      t.string :email,              :null => false, :default => ''
      t.string :encrypted_password, :null => false, :default => ''

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Omniauth login parameters
      t.string   :provider
      t.string   :uid

      ## Lockable
      # t.integer  :failed_attempts, :default => 0 # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      ## Token authenticatable
      # t.string :authentication_token
      t.timestamps
    end

    #Add Paperclip attachment
    add_attachment :users, :avatar

    add_index :users, :email,                :unique => true
    add_index :users, :reset_password_token, :unique => true
    add_index :users, :confirmation_token,   :unique => true
    # add_index :users, :unlock_token,         :unique => true
    # add_index :users, :authentication_token, :unique => true
  end
end
