class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :country_alpha2
      t.string :district_alpha2
      t.string :name
      t.string :name_alt, default: nil #alternative for international use: e.g.: name:lisboa name_alt:lisbon
      t.timestamps
    end
  end
end
