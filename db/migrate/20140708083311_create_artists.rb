class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
      t.integer :user_id
      t.integer :seller_id

      t.boolean :visible, default: true
      t.integer :subcategory_id

      t.string :name
      t.string :address
      t.string :country_alpha2 #the default country
      t.string :phone

      t.string :website

      t.string :external_icon
      t.attachment :icon

      t.string :external_photo
      t.attachment :photo

      t.boolean :trusted
      t.boolean :enable_crawler

      t.string :crawler_feed_url

      t.timestamps
    end
  end
end
