class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.integer :user_id
      t.integer :seller_id
      t.boolean :visible, default: true

      t.string :name
      t.string :address
      t.string :phone

      t.integer :city_id
      t.string :website

      t.string :external_icon
      t.attachment :icon

      t.string :external_photo
      t.attachment :photo

      t.float :latitude
      t.float :longitude

      t.boolean :trusted, default: false
      t.boolean :enable_crawler, default: false

      t.string :crawler_feed_url

      t.timestamps
    end
  end
end
