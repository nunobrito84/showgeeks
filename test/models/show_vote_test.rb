require 'test_helper'

class ShowVoteTest < ActiveSupport::TestCase
  fixtures :users, :shows

  test 'can register vote' do
    @andrew = users(:andrew)
    @show = shows(:show_normal_2)

    showVote = ShowVote.new()
    assert showVote.invalid?
    assert showVote.errors[:user_id].any?
    assert showVote.errors[:show_id].any?
    assert showVote.errors[:vote_type].any?
    assert_equal showVote.errors.size, 3
    showVote = ShowVote.new(user: @andrew, show: @show, vote_type: ShowVote::TYPE_COMPLAINT_SPAM)
    assert showVote.valid?
  end

  test 'user cannot vote twice in the same show, with the same vote_type' do
    @andrew = users(:andrew)
    @show_one = shows(:show_normal)
    showVote = ShowVote.new(user: @andrew, show: @show_one, vote_type: ShowVote::TYPE_ATTENDING)
    assert showVote.invalid?
    assert showVote.errors[:user_id].any?
    assert_equal showVote.errors.size, 1
  end

  test 'vote type must be valid' do
    @andrew = users(:andrew)
    @show_one = shows(:show_normal)
    showVote = ShowVote.new(user: @andrew, show: @show_one, vote_type: 9999)
    assert showVote.invalid?
    assert showVote.errors[:vote_type].any?
    assert_equal showVote.errors.size, 1
  end

  test 'who is attending the show' do
    @show_one = shows(:show_normal)
    assert_equal ShowVote.this_show(@show_one).attending.size, 2
  end
  test 'who is complaining wrong info' do
    @show_one = shows(:show_normal)
    assert_equal ShowVote.this_show(@show_one).complaining_wrong_info.size, 1
  end
  test 'who is complaining spam' do
    @show_one = shows(:show_normal)
    assert_equal ShowVote.this_show(@show_one).complaining_spam.size, 2
  end
  test 'who is complaining abuse' do
    @show_one = shows(:show_normal)
    assert_equal ShowVote.this_show(@show_one).complaining_abuse.size, 1
  end
end
