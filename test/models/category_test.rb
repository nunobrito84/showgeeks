require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  fixtures :categories, :subcategories

  test 'can create a category' do
    cat = Category.new()
    assert cat.invalid?
    assert cat.errors[:name].any?
    assert_equal cat.errors.size, 1
    cat = Category.new(name: 'category_example')
    assert cat.valid?
  end

  test 'category name is unique' do
    @music = categories(:music)
    cat = Category.new(name: @music.name)
    assert cat.invalid?
    assert cat.errors[:name].any?
    assert_equal cat.errors.size, 1
  end

  test 'category delete will also delete its subcategories' do
    @music = categories(:music)
    assert_equal @music.subcategories.size, 3
    assert_difference 'Subcategory.count', -3 do
      @music.destroy
    end
  end
end
