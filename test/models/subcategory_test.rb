require 'test_helper'

class SubcategoryTest < ActiveSupport::TestCase
  fixtures :categories, :subcategories

  test 'can create a subcategory' do
    subcat = Subcategory.new()
    assert subcat.invalid?
    assert subcat.errors[:category_id].any?
    assert subcat.errors[:name].any?
    assert_equal subcat.errors.size, 2
    subcat = Subcategory.new(category: categories(:music), name: 'subcategory_example')
    assert subcat.valid?
  end

  test 'subcategory name is unique' do
    @poprock = subcategories(:poprock)
    subcat = Subcategory.new(category: @poprock.category, name: @poprock.name)
    assert subcat.invalid?
    assert subcat.errors[:name].any?
    assert_equal subcat.errors.size, 1
  end

end
