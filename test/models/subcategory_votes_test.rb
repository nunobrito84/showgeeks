require 'test_helper'

class SubcategoryVotesTest < ActiveSupport::TestCase
  fixtures :users, :shows, :categories, :subcategories

  test 'can register subcategory vote' do
    @andrew = users(:andrew)
    @show = shows(:show_normal_2)

    subcatVote = SubcategoryVote.new()
    assert subcatVote.invalid?
    assert subcatVote.errors[:user_id].any?
    assert subcatVote.errors[:show_id].any?
    assert_equal subcatVote.errors.size, 2
    showVote = SubcategoryVote.new(user: @andrew, show: @show)
    assert showVote.valid?
  end

  test 'user cannot vote the subcategory twice in the same show' do
    @andrew = users(:andrew)
    @show_one = shows(:show_normal)
    subcatVote = SubcategoryVote.new(user: @andrew, show: @show_one)
    assert subcatVote.invalid?
    assert subcatVote.errors[:user_id].any?
    assert_equal subcatVote.errors.size, 1
  end

end
