require 'test_helper'

class ShowTest < ActiveSupport::TestCase
  fixtures :users, :shows

  test 'show basic creation' do
    @andrew = users(:andrew)

    show = Show.new()
    assert show.invalid?
    assert show.errors[:user_id].any?
    assert show.errors[:name].any?
    assert show.errors[:venue].any?

    assert show.errors[:date].any?
    assert show.errors[:time].any?
    assert show.errors[:country_alpha2].any?
    assert show.errors[:district_alpha2].any?
    assert show.errors[:city_id].any?
    assert_equal show.errors.size, 8
    show = Show.new(user: @andrew, name: 'Oasis', venue:'Pavilhão atlantico', date:'2014-12-31', time:'21:00:00', country_alpha2: 'PT', district_alpha2: '03', city_id: 41)
    assert show.valid?
  end

  test 'dont allow previous or distant dates' do
    @andrew = users(:andrew)
    show = Show.new(user: @andrew, name: 'Oasis', venue:'Pavilhão atlantico', date:'2010-01-01', time:'21:00:00', country_alpha2: 'PT', district_alpha2: '03', city_id: 41)
    assert show.invalid?
    assert show.errors[:date].any?
    assert_equal show.errors.size, 1
    show = Show.new(user: @andrew, name: 'Oasis', venue:'Pavilhão atlantico', date:'2012-01-01', time:'21:00:00', country_alpha2: 'PT', district_alpha2: '03', city_id: 41)
    assert show.invalid?
    assert show.errors[:date].any?
    assert_equal show.errors.size, 1
  end

  test 'dont allow duplicated events' do
    @show_one = shows(:show_normal)
    show = Show.new(user: @show_one.user, name: @show_one.name, venue:@show_one.venue, date: @show_one.date, time:@show_one.time, country_alpha2: @show_one.country_alpha2, district_alpha2: @show_one.district_alpha2, city_id: @show_one.city_id)
    assert show.invalid?
    assert show.errors[:name].any?
    assert_equal show.errors.size, 1
  end

  test 'check for existent country_district_city combination' do
    @show_one = shows(:show_normal)
    #Country err
    show = Show.new(user: @show_one.user, name: @show_one.name, venue:@show_one.venue, date: @show_one.date, time:@show_one.time, country_alpha2: 'PTXX', district_alpha2: @show_one.district_alpha2, city_id: @show_one.city_id)
    assert show.invalid?
    assert show.errors[:country_alpha2].any?
    assert_equal show.errors.size, 1
    #District err
    show = Show.new(user: @show_one.user, name: @show_one.name, venue:@show_one.venue, date: @show_one.date, time:@show_one.time, country_alpha2: @show_one.country_alpha2, district_alpha2: 'xxx', city_id: @show_one.city_id)
    assert show.invalid?
    assert show.errors[:district_alpha2].any?
    assert_equal show.errors.size, 1
    #City err
    show = Show.new(user: @show_one.user, name: @show_one.name, venue:@show_one.venue, date: @show_one.date, time:@show_one.time, country_alpha2: @show_one.country_alpha2,  district_alpha2: @show_one.district_alpha2, city_id: 99999)
    assert show.invalid?
    assert show.errors[:city_id].any?
    assert_equal show.errors.size, 1
  end

  test 'can find the shows from a country/district' do
    assert_equal Show.from_country('PT').from_district('03').size, 2
  end

  test 'upload a photo' do
    @andrew = users(:andrew)
    @normal_image = File.new( Rails.root + 'test/fixtures/images/test_icon.png' )
    show = Show.new(user: @andrew, name: 'Oasis', photo: @normal_image, venue:'Pavilhão atlantico', date:'2014-12-31', time:'21:00:00', country_alpha2: 'PT', district_alpha2: '03', city_id: 41)
    assert show.valid?
  end

end
