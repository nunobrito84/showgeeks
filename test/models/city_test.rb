require 'test_helper'

class CityTest < ActiveSupport::TestCase
  fixtures :cities

  test 'can register city' do
    city = City.new()
    assert city.invalid?
    assert city.errors[:name].any?
    assert city.errors[:country_alpha2].any?
    assert city.errors[:district_alpha2].any?
    assert_equal city.errors.size, 3
    city = City.new(name: 'example', country_alpha2: 'PT', district_alpha2: '03')
    assert city.valid?
  end
  test 'country exists' do
    city = City.new(name: 'Barcelos', country_alpha2: 'xxx', district_alpha2: '03')
    assert city.invalid?
    assert city.errors[:country_alpha2].any?
    assert_equal city.errors.size, 1
  end
  test 'district exists for this country' do
    city = City.new(name: 'Barcelos', country_alpha2: 'PT', district_alpha2: 'xxx')
    assert city.invalid?
    assert city.errors[:district_alpha2].any?
    assert_equal city.errors.size, 1
  end
  test 'city must be unique inside its country and district' do
    city = City.new(name: 'Barcelos', country_alpha2: 'PT', district_alpha2: '03')
    assert city.invalid?
    assert city.errors[:name].any?
    assert_equal city.errors.size, 1
  end
end
