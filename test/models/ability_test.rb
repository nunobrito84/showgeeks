require 'test_helper'

class AbilityTest < ActiveSupport::TestCase
  setup do
    @andrew = users(:andrew)
    @thomas = users(:thomas)
    @nuno = users(:nuno)
  end

  test 'what public users can do' do
    ability = Ability.new(User.new)
    assert ability.cannot?(:new, User)
    assert ability.cannot?(:manage, User)
    assert ability.cannot?(:edit, @thomas)
    assert ability.can?(:limits, User)
    assert ability.cannot?(:show_user_extra_data, User)
  end

  test 'what normal users can do' do
    ability = Ability.new(@andrew)
    assert ability.cannot?(:new, User)
    assert ability.cannot?(:manage, User)
    assert ability.cannot?(:show_user_extra_data, User)

    assert ability.can?(:show, @andrew)
    assert ability.cannot?(:edit, @andrew)

    assert ability.cannot?(:show, @thomas)
    assert ability.cannot?(:edit, @thomas)


  end

  test 'what admin users can do' do
    ability = Ability.new(@nuno)
    assert ability.can?(:manage, User)
    assert ability.can?(:edit, @andrew)
    assert ability.can?(:show_user_extra_data, User)
  end

end