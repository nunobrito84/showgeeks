require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'user basic account creation' do
    user = User.new()
    assert user.invalid?
    assert user.errors[:name].any?
    assert user.errors[:email].any?
    assert user.errors[:password].any?
    assert user.errors[:country_alpha2].empty?
    assert user.errors[:district_alpha2].empty?
    assert user.errors[:city_id].empty?
    assert_equal(user.errors, 6);

    user = User.new(name: 'user', email: 'abcd@abcd.com', password: 'abcdefgh', password_confirmation: 'abcdefgh', country_alpha2: 'PT', district_alpha2: '03', city_id: 1)
    assert user.valid?
    assert user.admin == false
    assert user.trust == 1
  end

end
