require 'test_helper'

class ArtistsControllerTest < ActionController::TestCase
  setup do
    @artist = artists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:artists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create artist" do
    assert_difference('Artist.count') do
      post :create, artist: { address: @artist.address, crawler_feed_url: @artist.crawler_feed_url, enable_crawler: @artist.enable_crawler, external_icon: @artist.external_icon, external_photo: @artist.external_photo, icon: @artist.icon, name: @artist.name, phone: @artist.phone, photo: @artist.photo, public: @artist.public, trusted: @artist.trusted, user_id: @artist.user_id, website: @artist.website }
    end

    assert_redirected_to artist_path(assigns(:artist))
  end

  test "should show artist" do
    get :show, id: @artist
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @artist
    assert_response :success
  end

  test "should update artist" do
    patch :update, id: @artist, artist: { address: @artist.address, crawler_feed_url: @artist.crawler_feed_url, enable_crawler: @artist.enable_crawler, external_icon: @artist.external_icon, external_photo: @artist.external_photo, icon: @artist.icon, name: @artist.name, phone: @artist.phone, photo: @artist.photo, public: @artist.public, trusted: @artist.trusted, user_id: @artist.user_id, website: @artist.website }
    assert_redirected_to artist_path(assigns(:artist))
  end

  test "should destroy artist" do
    assert_difference('Artist.count', -1) do
      delete :destroy, id: @artist
    end

    assert_redirected_to artists_path
  end
end
