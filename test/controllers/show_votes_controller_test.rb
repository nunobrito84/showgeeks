require 'test_helper'

class ShowVotesControllerTest < ActionController::TestCase
  setup do
    @show_vote = show_votes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:show_votes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create show_vote" do
    assert_difference('ShowVote.count') do
      post :create, show_vote: { amount: @show_vote.amount, show_id: @show_vote.show_id, vote_type: @show_vote.vote_type, user_id: @show_vote.user_id }
    end

    assert_redirected_to show_vote_path(assigns(:show_vote))
  end

  test "should show show_vote" do
    get :show, id: @show_vote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @show_vote
    assert_response :success
  end

  test "should update show_vote" do
    patch :update, id: @show_vote, show_vote: { amount: @show_vote.amount, show_id: @show_vote.show_id, vote_type: @show_vote.vote_type, user_id: @show_vote.user_id }
    assert_redirected_to show_vote_path(assigns(:show_vote))
  end

  test "should destroy show_vote" do
    assert_difference('ShowVote.count', -1) do
      delete :destroy, id: @show_vote
    end

    assert_redirected_to show_votes_path
  end
end
