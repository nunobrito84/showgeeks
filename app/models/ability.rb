class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    p 'cancan ability executing...'

    if user.admin?
      can :manage, :all
      can :create, :crawler #This must be a symbol because Crawler has no model class

    elsif(user.id) #Logged user
      can [:edit, :update], User, :id => user.id #can only edit its own user data
      if(user.is_seller) #is a seller
        can :index, User
      else #is a regular user
        can :edit, Show, :user => user #can only edit its own shows
        can :new, Show
      end

    end

    #Public user
    can [:index], :district #This must be a symbol because District has no model class
    can [:index], City
    can [:show], Venue, visible: true
    can [:show], Artist, visible: true
    can [:index, :show], Show
    can [:show, :index, :reload_shows, :reload_suggestion], Show

  end
end
