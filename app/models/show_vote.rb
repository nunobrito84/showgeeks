class ShowVote < ActiveRecord::Base
  belongs_to :user
  belongs_to :show

  VOTE_TYPES = [
      TYPE_ATTENDING = 1,
      TYPE_COMPLAINT_WRONG_INFO = 2,
      TYPE_COMPLAINT_SPAM = 3,
      TYPE_COMPLAINT_ABUSE = 4
  ]

  COMPLAINT_SHOW_THRESHOLD = 2 # Minimum amount of votes to show warnings to users

  BLUE_EVENT_ATTENDANCE_LEVEL = 10 # This is the level of attendance to turn the main list badge blue
  GREEN_EVENT_ATTENDANCE_LEVEL = 20 # This is the level of attendance to turn the main list badge green

  scope :attending, -> { where(vote_type: TYPE_ATTENDING) }
  scope :this_show, ->(show_id) { where(show_id: show_id) }

  scope :complaining_wrong_info, -> { where(vote_type: TYPE_COMPLAINT_WRONG_INFO) }
  scope :complaining_spam, -> { where(vote_type: TYPE_COMPLAINT_SPAM) }
  scope :complaining_abuse, -> { where(vote_type: TYPE_COMPLAINT_ABUSE) }

  #Unique pair: user and show
  validates_presence_of :user_id, :show_id, :vote_type
  validates_uniqueness_of :user_id, :scope => [:show_id, :vote_type]
  validates_inclusion_of :vote_type, :in => VOTE_TYPES, :if => :vote_type
end
