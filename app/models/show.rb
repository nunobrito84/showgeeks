class Show < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  belongs_to :user
  belongs_to :category
  belongs_to :subcategory
  belongs_to :city

  belongs_to :venue
  belongs_to :artist

  has_many :subcategory_votes
  has_many :show_votes

  NOT_TRUSTED_SOURCE_LEVEL = 1
  TRUSTED_SOURCE_LEVEL = 101

  VISIBLE_YES = 1
  VISIBLE_NO = 0
  VISIBLE_DONT_KNOW = -1
  VISIBLE_IGNORE = -2
  VISIBLE_OPTIONS = [VISIBLE_YES, VISIBLE_NO, VISIBLE_DONT_KNOW, VISIBLE_IGNORE]

  has_attached_file :photo,
                    :styles => { :medium => "200x200>", :thumb => "50x50>" },
                    :default_url => "/assets/shows/missing.png",
                    :url  => "/system/:class/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
                    #:path => ":rails_root/public/assets/shows/:id/photo/:style/:basename.:extension"
  #validates_attachment_presence :avatar
  validates_attachment_size :photo, :less_than => 500.kilobytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png']

  scope :from_country, lambda {|country_alpha2| where(country_alpha2: country_alpha2)}
  scope :from_district, lambda {|district_alpha2| where(district_alpha2: district_alpha2)}

  validates_uniqueness_of :name, :scope => [:venue_name, :date, :country_alpha2, :district_alpha2, :city_id]
  validates_presence_of :user_id, :name, :date, :country_alpha2, :district_alpha2, :city_id, :city_name, :venue_name
  validates_inclusion_of :visible, in: VISIBLE_OPTIONS
      #validates_associated :venue, :message => 'This venue is invalid'

  validates_length_of :price, :maximum => 10, :allow_blank => true

  # Don't let dates be past dates
  validate :date_actual_or_future
  def date_actual_or_future
    if self.date
      if self.date < Date.today
        self.errors.add(:date, I18n.t('activerecord.models.show.errors.past_date'))
      elsif self.date > 1.years.from_now
        self.errors.add(:date, I18n.t('activerecord.models.show.errors.distant_date'))
      end
    end
  end

  #Check if the combination of country, district and city are valid. If javascript has some error, it can happen!
  validate :check_country_district_city_combination
  def check_country_district_city_combination
    if !self.country_alpha2.blank? or self.district_alpha2 or self.city_id
      if !Country[self.country_alpha2] #First check for country
        self.errors.add(:country_alpha2, I18n.t('activerecord.models.show.errors.invalid_country'))
      else #Then check for district
        if !(Country[self.country_alpha2].states.include? self.district_alpha2)
          self.errors.add(:district_alpha2, I18n.t('activerecord.models.show.errors.invalid_district'))
        else #Then check for city
          if !City.from_country(self.country_alpha2).from_district(self.district_alpha2).exists?(self.city_id)
            self.errors.add(:city_id, I18n.t('activerecord.models.show.errors.invalid_city'))
          end
        end
      end
    else
      self.errors.add(:country_alpha2, I18n.t('activerecord.models.show.errors.invalid_country'))
    end
  end

  #  onDestroy:
  #    - destroy subcategory_voter
  #    - destroy

  before_validation do #probable change to before_validation
    #Determine which are the country, district and city of the codes
    if self.city_id.is_a? Numeric
      self.city_name = City.find(self.city_id).name
    end

    #Only apply the denormalization over the venue, IF the venue is public. Otherwise it must be the responsability of the crawler itself
    if(self.venue and self.venue.visible)
      venue = Venue.find(self.venue_id)
      self.venue_name = venue.name if self.venue_name.blank?
      self.venue_website = venue.website if self.venue_website.blank?
      if self.venue_icon.blank?
        self.venue_icon = !venue.external_icon.nil? ? venue.external_icon : venue.icon.url
      end
    end
  end

  # Try building a slug based on the following fields in increasing order of specificity.
  def slug_candidates
    [
        :name,
        [:city_name, :name],
        [:district_name, :city_name, :name],
        [:country_name, :district_name, :city_name, :name]
    ]
  end
  def city_name
    self.city.try(:name)
  end
  def district_name
    self.city.district_name if self.city
  end
  def country_name
    self.city.country_name if self.city
  end


end
