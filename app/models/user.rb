class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable,
  # :lockable, :timeoutable and :omniauthable
  devise :omniauthable, :omniauth_providers => [:facebook]
  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      Rails.logger.info "Auth variable inspect: #{auth.inspect}"
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.avatar = process_uri(auth.info.image) # assuming the user model has an image
      user.skip_confirmation! #For facebook authentication, no need for confirmation
    end
  end

  private
  def self.process_uri(uri)
    avatar_url = URI.parse(uri)
    avatar_url.scheme = 'https'
    avatar_url.to_s
  end

  public
  has_many :shows
  has_many :subcategory_votes
  has_many :show_votes
  has_many :venues

  belongs_to :seller, :class_name => 'User', :foreign_key => 'seller_id'
  has_many :clients, :class_name => 'User', :foreign_key => 'seller_id'

  attr_accessor :affiliate_email
  # If there is a affiliate, it must be a valid seller
  validate :affiliate_validation
  private
  def affiliate_validation
    if !self.try(:affiliate_email).blank?  #in a omniauth login affiliate_email is non-existent
      affiliate_user = User.find_by(email: self.affiliate_email)#.try(:seller)
      if affiliate_user.nil?
        self.errors.add(:affiliate_email, "#{I18n.t('activerecord.models.user.errors.invalid_user')} (0)")
        return
      elsif !affiliate_user.is_seller
        self.errors.add(:affiliate_email, "#{I18n.t('activerecord.models.user.errors.invalid_user')} (1)")
        return
      end
    end
  end

  before_save do
    if self.try(:affiliate_email) #in a omniauth login affiliate_email is non-existent
      self.seller_id = User.find_by(email: self.affiliate_email).try(:id)
    end
  end

  has_attached_file :avatar,
                    :styles => { :medium => "300x300>", :thumb => "30x30>" },
                    :default_url => "/assets/users/avatar/:style/missing.png",
                    :url  => "/system/:class/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
                    #:path => ":rails_root/public/assets/users/:id/:style/:basename.:extension"

  validates_attachment_size :avatar, :less_than => 50.kilobytes
  validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png']

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_uniqueness_of :email
  validates_presence_of :name
end
