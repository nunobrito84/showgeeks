class Artist < ActiveRecord::Base
  include CountriesHelper

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  belongs_to :user
  belongs_to :subcategory

  has_many :shows
  has_many :artist_crawler_reports

  validates_presence_of :user, :name, :website
  validates_presence_of :crawler_feed_url, :if => :enable_crawler
  validates_uniqueness_of :name, :message => I18n.t('activerecord.models.artist.errors.unique_artist')

  validates_inclusion_of :country_alpha2, in: Country.all.map{|c| c[1]}
  validate :valid_crawler, :if => :enable_crawler

  private
  def valid_crawler
    errors.add(:crawler_feed_url, I18n.t('activerecord.models.artist.errors.invalid_crawler')) unless IArtistCrawler::RECOGNIZED_CRAWLERS.include?(ICrawler.get_crawler_class(self.crawler_feed_url))
  end

  has_attached_file :photo,
                    :styles => { :medium => "200x200>", :thumb => "50x50>" },
                    :default_url => "/assets/artists/missing.jpg",
                    :url  => "/system/:class/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
                    #:path => ":rails_root/public/assets/artists/:id/photo/:style/:basename.:extension"
  validates_attachment_size :photo, :less_than => 250.kilobytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png']
  validates_presence_of :photo, if: Proc.new{|f| f.external_photo.nil? or f.external_photo.blank? }

  validates_presence_of :subcategory

  has_attached_file :icon,
                    :default_url => "/assets/venues/missing.png",
                    :url  => "/system/:class/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
                    #:path => ":rails_root/public/assets/venues/:id/icon/:style/:basename.:extension"
  validates_attachment_size :icon, :less_than => 3.kilobytes
  validates_attachment_content_type :icon, :content_type => ['image/jpeg', 'image/png']
  #validates_presence_of :icon, if: Proc.new{|f| f.external_icon.nil? or f.external_icon.blank? }

  validate :icon_dimensions
  private
  def icon_dimensions
    required_width  = 16
    required_height = 16
    #p icon.inspect
    if(icon.queued_for_write[:original])
      dimensions = Paperclip::Geometry.from_file(icon.queued_for_write[:original].path)
      errors.add(:icon, "#{I18n.t('activerecord.models.artist.errors.invalid_width')} #{required_width}px") unless dimensions.width == required_width
      errors.add(:icon, "#{I18n.t('activerecord.models.artist.errors.invalid_height')} #{required_height}px") unless dimensions.height == required_height
    end
  end

  public
  def photo_url
    if !self.external_photo.blank?
      self.external_photo
    else
      self.photo.url(:medium)
    end
  end

  def icon_url
    if !self.external_icon.blank?
      self.external_icon
    else
      self.icon.url
    end
  end

  after_update do #update the denormalized attributes in the shows list
    if(self.name_changed? or self.icon_file_name_changed? or self.external_icon_changed?)
      self.shows.update_all(name: name, artist_icon: icon_url)
    end
  end

  # Try building a slug based on the following fields in increasing order of specificity.
  def slug_candidates
    [
        :name,
        [:country_name, :name]
    ]
  end
  def country_name
    Country[self.country_alpha2].name if self.country_alpha2
  end

end
