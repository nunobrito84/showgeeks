class VenueCrawlerReport < ActiveRecord::Base
  belongs_to :venue

  LAST_REPORTS_KEPT_NUMBER = 10

  validates_presence_of :duration

  after_save :remove_excess_reports
  private
  def remove_excess_reports
    VenueCrawlerReport.where(venue_id: self.venue.id).order(id: :desc).offset(LAST_REPORTS_KEPT_NUMBER).destroy_all
  end

end
