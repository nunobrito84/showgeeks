class Venue < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  belongs_to :city
  belongs_to :user

  has_many :shows
  has_many :venue_crawler_reports

  scope :from_city, lambda {|city_id| where(city_id: city_id)}

  validates_presence_of :user, :name, :address, :city, :website
  validates_presence_of :crawler_feed_url, :if => :enable_crawler
  validates_uniqueness_of :name, :scope => :city_id, :message => I18n.t('activerecord.models.venue.errors.unique_venue')

  validate :valid_crawler, :if => :enable_crawler
  private
  def valid_crawler
    errors.add(:crawler_feed_url, I18n.t('activerecord.models.venue.errors.invalid_crawler')) unless IVenueCrawler::RECOGNIZED_CRAWLERS.include?(ICrawler.get_crawler_class(self.crawler_feed_url))
  end

  has_attached_file :photo,
                    :styles => { :medium => "200x200>", :thumb => "50x50>" },
                    :default_url => "/assets/shows/missing.png",
                    :url  => "/system/:class/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
                    #:path => ":rails_root/public/assets/venues/:id/photo/:style/:basename.:extension"
  validates_attachment_size :photo, :less_than => 250.kilobytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png']
  validates_presence_of :photo, if: Proc.new{|f| f.external_photo.nil? or f.external_photo.blank? }

  has_attached_file :icon,
                    :default_url => "/assets/venues/missing.png",
                    :url  => "/system/:class/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
                    #:path => ":rails_root/public/assets/venues/:id/icon/:style/:basename.:extension"
  validates_attachment_size :icon, :less_than => 3.kilobytes
  validates_attachment_content_type :icon, :content_type => ['image/jpeg', 'image/png']
  #validates_presence_of :icon, if: Proc.new{|f| f.external_icon.nil? or f.external_icon.blank? }

  validate :icon_dimensions

  # Try building a slug based on the following fields in increasing order of specificity.
  def slug_candidates
    [
        :name,
        [:city_name, :name],
        [:district_name, :city_name, :name],
        [:country_name, :district_name, :city_name, :name]
    ]
  end

  private
  def icon_dimensions
    required_width  = 16
    required_height = 16
    #p icon.inspect
    if(icon.queued_for_write[:original])
      dimensions = Paperclip::Geometry.from_file(icon.queued_for_write[:original].path)
      errors.add(:icon, "#{I18n.t('activerecord.models.venue.errors.invalid_width')} #{required_width}px") unless dimensions.width == required_width
      errors.add(:icon, "#{I18n.t('activerecord.models.venue.errors.invalid_height')} #{required_height}px") unless dimensions.height == required_height
    end
  end

  public
  def photo_url
    if !self.external_photo.blank?
      self.external_photo
    else
      self.photo.url(:medium)
    end
  end

  def icon_url
    if !self.external_icon.blank?
      self.external_icon
    else
      self.icon.url
    end
  end

  after_update do #update the denormalized attributes in the shows list
     if(self.name_changed? or self.website_changed? or self.icon_file_name_changed? or self.external_icon_changed?)
       self.shows.update_all(venue_name: name, venue_website: website, venue_icon: icon_url)
     end
  end

  def city_name
    self.city.try(:name)
  end
  def district_name
    self.city.district_name if self.city
  end
  def country_name
    self.city.country_name if self.city
  end

=begin
  Facebook 156 categories as of June 3rd, 2014, API v2.0
  1103, Actor/Director
  1105, Movie
  1108, Producer
  1109, Writer
  1110, Studio
  1111, Movie Theater
  1111, Movie Theater
  1112, TV/Movie Award
  1113, Fictional Character
  1113, Fictional Character
  1114, Movie Character
  1114, Movie Character
  1200, Album
  1201, Song
  1202, Musician/Band
  1207, Music Video
  1208, Concert Tour
  1209, Concert Venue
  1209, Concert Venue
  1210, Radio Station
  1211, Record Label
  1212, Music Award
  1213, Music Chart
  1300, Book
  1301, Author
  1305, Book Store
  1305, Book Store
  1306, Library
  1306, Library
  1307, Magazine
  1309, Book Series
  1400, TV Show
  1402, TV Network
  1404, TV Channel
  1600, Athlete
  1601, Artist
  1602, Public Figure
  1604, Journalist
  1605, News Personality
  1606, Chef
  1607, Lawyer
  1608, Doctor
  1609, Business Person
  1610, Comedian
  1611, Entertainer
  1613, Teacher
  1614, Dancer
  1615, Designer
  1616, Photographer
  1617, Entrepreneur
  1700, Politician
  1701, Government Official
  1800, Sports League
  1801, Professional Sports Team
  1802, Coach
  1803, Amateur Sports Team
  1804, School Sports Team
  1900, Restaurant/Cafe
  2100, Bar
  2101, Club
  2200, Company
  2201, Product/Service
  2202, Website
  2205, Cars
  2206, Bags/Luggage
  2208, Camera/Photo
  2209, Clothing
  2210, Computers
  2211, Software
  2212, Office Supplies
  2213, Electronics
  2214, Health/Beauty
  2215, Appliances
  2216, Building Materials
  2217, Commercial Equipment
  2218, Home Decor
  2219, Furniture
  2220, Household Supplies
  2221, Kitchen/Cooking
  2222, Patio/Garden
  2223, Tools/Equipment
  2224, Wine/Spirits
  2226, Jewelry/Watches
  2230, Pet Supplies
  2231, Outdoor Gear/Sporting Goods
  2232, Baby Goods/Kids Goods
  2233, Media/News/Publishing
  2234, Bank/Financial Institution
  2235, Non-Governmental Organization (NGO)
  2236, Insurance Company
  2237, Small Business
  2238, Energy/Utility
  2239, Retail and Consumer Merchandise
  2240, Automobiles and Parts
  2241, Industrials
  2242, Transport/Freight
  2243, Health/Medical/Pharmaceuticals
  2244, Aerospace/Defense
  2245, Mining/Materials
  2246, Farming/Agriculture
  2247, Chemicals
  2248, Consulting/Business Services
  2249, Legal/Law
  2250, Education
  2251, Engineering/Construction
  2252, Food/Beverages
  2253, Telecommunication
  2254, Biotechnology
  2255, Computers/Technology
  2256, Internet/Software
  2258, Travel/Leisure
  2260, Community Organization
  2261, Political Organization
  2262, Vitamins/Supplements
  2263, Drugs
  2264, Church/Religious Organization
  2265, Phone/Tablet
  2300, Games/Toys
  2301, App Page
  2302, Video Game
  2303, Board Game
  2500, Local Business
  2501, Hotel
  2503, Landmark
  2506, Airport
  2507, Sports Venue
  2508, Arts/Entertainment/Nightlife
  2509, Automotive
  2510, Spas/Beauty/Personal Care
  2511, Event Planning/Event Services
  2512, Bank/Financial Services
  2513, Food/Grocery
  2514, Health/Medical/Pharmacy
  2515, Home Improvement
  2516, Pet Services
  2517, Professional Services
  2518, Business Services
  2519, Community/Government
  2520, Real Estate
  2521, Shopping/Retail
  2522, Public Places
  2523, Attractions/Things to Do
  2524, Sports/Recreation/Activities
  2525, Tours/Sightseeing
  2526, Transportation
  2527, Hospital/Clinic
  2528, Museum/Art Gallery
  2600, Organization
  2601, School
  2602, University
  2603, Non-Profit Organization
  2604, Government Organization
  2606, Cause
  2618, Political Party
  2632, Pet
  2637, Middle School
=end

end
