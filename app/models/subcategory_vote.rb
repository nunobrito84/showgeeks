class SubcategoryVote < ActiveRecord::Base
  belongs_to :user
  belongs_to :show
  belongs_to :subcategory

  validates_presence_of :user_id, :show_id
  #Unique pair: user and show
  validates_uniqueness_of :user_id, :scope => [:show_id]

  MINIMUM_AMOUNT_OF_VOTES = 10 #This is the amount of votes I am happy with

end
