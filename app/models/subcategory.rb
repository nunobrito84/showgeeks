class Subcategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  belongs_to :category
  has_many :subcategory_votes
  has_many :artists

  validates_presence_of :category_id, :name
  validates_uniqueness_of :name

  # Try building a slug based on the following fields in increasing order of specificity.
  def slug_candidates
    [   :name,
        [:category_id, :name]
    ]
  end

end
