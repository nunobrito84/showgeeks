class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  has_many :subcategories, dependent: :destroy

  validates :name, presence: true, uniqueness: true


  All = nil
  Music = 1
  Theatre = 2
  Sport = 5

  DEFAULT_CATEGORY = '1'

  # Try building a slug based on the following fields in increasing order of specificity.
  def slug_candidates
    [   :name   ]
  end

end
