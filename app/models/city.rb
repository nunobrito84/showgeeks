
class City < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  validates_uniqueness_of :name, :scope => [:country_alpha2, :district_alpha2] #Unique key pair

  validates_presence_of :name, :country_alpha2, :district_alpha2 #Country and district must exist

  scope :from_country, lambda {|country_alpha2| where(country_alpha2: country_alpha2)}
  scope :from_district, lambda {|district_alpha2| where(district_alpha2: district_alpha2)}

  #Check if the combination of country, district and city are valid. If javascript has some error, it can happen!
  validate :check_country_district_combination, :if => :country_alpha2 and :district_alpha2
  def check_country_district_combination

    #p "ctry: #{Country[self.country_alpha2]}"
    if !Country[self.country_alpha2] #First check for country
      self.errors.add(:country_alpha2, I18n.t('activerecord.models.city.errors.invalid_country'))
    else #Then check for district
      if !(Country[self.country_alpha2].states.include? self.district_alpha2)
        self.errors.add(:district_alpha2, I18n.t('activerecord.models.city.errors.invalid_country'))
      end
    end
  end

  def country_name
    Country[self.country_alpha2].name if self.country_alpha2
  end
  def district_name
    Country[self.country_alpha2].states[self.district_alpha2]['name'] if self.country_alpha2 and self.district_alpha2
  end

  # Try building a slug based on the following fields in increasing order of specificity.
  def slug_candidates
    [
        :name,
        [:district_name, :name],
        [:country_name, :district_name, :name]
    ]
  end

end
