json.array!(@artists) do |artist|
  json.extract! artist, :id, :visible, :user_id, :name, :address, :phone, :website, :external_icon, :icon, :external_photo, :photo, :trusted, :enable_crawler, :crawler_feed_url
  json.url artist_url(artist, format: :json)
end
