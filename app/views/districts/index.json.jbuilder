json.array!(@districts) do |district|
  json.alpha2 district[0]
  json.name district[1]['name']
  #json.url country_district_url(@country.alpha2, district[0], format: :json)
end
