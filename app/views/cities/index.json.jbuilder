json.array!(@cities) do |city|
  json.extract! city, :id, :country_alpha2, :district_alpha2, :name
  #json.url country_district_city_url(@country.alpha2, @district[:alpha2], city, format: :json)
end
