json.array!(@cities) do |city|
  json.extract! city, :code, :name
end