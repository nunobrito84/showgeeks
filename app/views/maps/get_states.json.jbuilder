json.array!(@states) do |state|
  json.extract! state, :code, :name
end