json.array!(@countries) do |country|
  json.alpha2 country.alpha2
  json.name country.name
  json.url country_url(country.alpha2, format: :json)
end
