json.array!(@show_votes) do |show_vote|
  json.extract! show_vote, :user_id, :show_id, :vote_type, :amount, :info
  json.url show_vote_url(show_vote, format: :json)
end
