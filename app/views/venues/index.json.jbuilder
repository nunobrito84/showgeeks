json.array!(@venues) do |venue|
  json.extract! venue, :id, :city_id, :name
end
