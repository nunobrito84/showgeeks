class MapsController < ApplicationController
  load_and_authorize_resource :except => [:get_states, :get_cities]

  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  def get_states
    begin
      puts 'STATES JSON: ' + params[:state_id]
      @states = Country.find_by_code(params[:state_id]).districts
      puts "Found #{@states.length} states for #{params[:state_id]}"
      respond_to do |format|
        format.html { render :partial => 'maps/districts', :locals => { :states => @states } }
        format.json { render json: @states }
      end
    rescue
      return nil
    end
  end

  def get_cities
    begin
      @cities = District.find_by_code(params[:district_id]).cities
      puts "Found #{@cities.length} cities for #{params[:district_id]}"
      respond_to do |format|
        format.json { render json: @cities }
      end
    rescue
      return nil
    end
  end

end
