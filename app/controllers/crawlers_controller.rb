class CrawlersController < ApplicationController
  authorize_resource :class => false #Because there is no model for this class

  #GET crawlers
  def index
    authorize! :index, :crawler
    @crawlers = []
  end

  #POST crawlers
  include CrawlersHelper
  def create
    authorize! :new, :crawler
    begin
      a = ICrawler.get_crawler_class(params[:artist_url])
      case params[:type].downcase
        when 'artist'
          case ICrawler.get_crawler_class(params[:artist_url])
            when ICrawler::FACEBOOK
              buildFacebookArtist(params[:artist_url])
            when ICrawler::BANDS_IN_TOWN
              buildBandsInTownArtist(params[:artist_url])
            else
              raise "Crawler class type is invalid: #{params[:artist_url]}"
          end
        when 'venue'
          buildFacebookVenue(params[:venue_url])
        else
          redirect_to root_path, notice: "#{params[:type]} is an unknown type"
      end
    rescue Exception => e
      respond_to do |format|
        p e.backtrace
        flash[:alert] = "Could not import data:#{e.message}\nBacktrace:\n#{e.backtrace[0..5].join('\n')}"
        format.html { redirect_to crawlers_path }
      end
    end
  end

  private
  def buildFacebookArtist(url)
    @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
    #Then, get the facebook name
    fb_user = ICrawler.parse_user_id(url)
    raise "Cannot parse the facebook username: #{fb_user}" if !fb_user
    p "Facebook user: #{fb_user}"
    #Then, get the facebook ID and venue parameters
    fb_user_info = @graph.get_connections(fb_user, '')
    raise "Cannot get facebook user info for: #{fb_user}" if !fb_user_info
    p "Facebook user info: #{fb_user_info['id']}"
    artist_name = fb_user_info['name']
    artist_phone = fb_user_info['phone']
    fb_user_website = fb_user_info['website'] || ''
    if fb_user_website.include? ';'
      artist_website = (fb_user_website.split(';').first)
    else #By default assume the space char as splitting the websites
      artist_website = (fb_user_website.split(' ').first)
    end
    artist_website = ('http://' + artist_website) if !artist_website.blank? and !url_protocol_present?(artist_website)

    artist_likes = fb_user_info['likes']
    artist_crawler = "https://www.facebook.com/#{fb_user_info['id']}"

    #Lets get the profile picture with 200x200
    fb_user_picture = @graph.get_picture(fb_user_info['id'], {height: '200', width: '200'})
    raise "Cannot get facebook user picture for artist: #{fb_user}" if !fb_user_picture
    artist_picture_url = fb_user_picture

    #Lets get the website icon
    artist_url = artist_website
    doc = Nokogiri::HTML(open(artist_url))
    icon_url = nil
    # Use an xpath expression to tell Nokogiri what to look for.
    doc.xpath('//link[@rel="shortcut icon"]').each do |tag|
      taguri = URI(tag['href'])
      unless taguri.host.to_s.length < 1
        # There is a domain name in taguri, so we're good
        icon_url = taguri.to_s
      else
        # So we have to join it with the index URL we built at the beginning of the method
        icon_url = URI.join(artist_url, taguri).to_s
      end
    end
    respond_to do |format|
      format.html { redirect_to new_artist_path(
                                    name: artist_name,
                                    address: '',
                                    phone: artist_phone,
                                    website: artist_website,

                                    external_photo: artist_picture_url,
                                    external_icon: icon_url,

                                    trusted: artist_likes > 10000,
                                    enable_crawler: artist_likes > 10000,
                                    crawler_feed_url: artist_crawler,
                                ), notice: 'Venue data imported.' }
    end
    #Then, get the icon by parsing the page using Nokogiri
    #Then, fill the form of venue#new and redirect to complete the insertion

  end

  private
  def buildBandsInTownArtist(url)
    bit_user_id = ICrawler.parse_bands_in_town_id(url)
    artist_crawler = "http://www.bandsintown.com/#{URI.encode(bit_user_id)}"

    raise "Cannot parse the facebook username: #{bit_user_id}" if !bit_user_id
    artist = Bandsintown::Artist.get({:name => bit_user_id})

    #p artist.facebook_tour_dates_url
    if artist.facebook_tour_dates_url.nil?
      respond_to do |format|
        format.html { redirect_to new_artist_path(
                                      name: artist.name,
                                      address: '',

                                      external_photo: artist.image_url,
                                      external_icon: artist.thumb_url,

                                      trusted: true,
                                      enable_crawler: true,
                                      crawler_feed_url: artist_crawler,
                                  ), notice: 'Venue data imported, but there is no facebook association.' }
      end
    else # there is a FB association... lets crawl over it
      bit_facebook_redirect_page = open(artist.facebook_tour_dates_url).base_uri.to_s # Now from here, we get the JS redirect address. example: window.location.replace('https://www.facebook.com/LilWayne?sk=app_123966167614127');
      doc = Nokogiri::XML(open(bit_facebook_redirect_page))
      fb_addresses = URI.extract(doc.text).select { |address| address.include? ICrawler::FACEBOOK }
      raise "No facebook address found in #{artist.facebook_tour_dates_url}" if fb_addresses.blank?
      raise "Multiple facebook addresses found in #{artist.facebook_tour_dates_url}" if fb_addresses.size > 1
      fb_user = ICrawler.parse_user_id(fb_addresses.first)
      raise "Cannot parse the facebook username: #{fb_user}" if !fb_user
      p "Facebook user: #{fb_user}"

      @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
                                                                                      #Then, get the facebook ID and venue parameters
      fb_user_info = @graph.get_connections(fb_user, '')
      raise "Cannot get facebook user info for: #{fb_user}" if !fb_user_info
      p "Facebook user info: #{fb_user_info['id']}"
      artist_name = fb_user_info['name']
      artist_phone = fb_user_info['phone']
      fb_user_website = fb_user_info['website'] || ''
      if fb_user_website.include? ';'
        artist_website = (fb_user_website.split(';').first)
      else #By default assume the space char as splitting the websites
        artist_website = (fb_user_website.split(' ').first)
      end
      artist_website = ('http://' + artist_website) if !artist_website.blank? and !url_protocol_present?(artist_website)

      artist_likes = fb_user_info['likes']

      #Lets get the profile picture with 200x200
      artist_photo = @graph.get_picture(fb_user_info['id'], {height: '200', width: '200'}) || artist.image_url
      raise "Cannot get facebook user picture for artist: #{fb_user}" if !artist_photo
      artist_picture_url = artist_photo

      #Lets get the website icon
      artist_url = artist_website
      doc = Nokogiri::HTML(open(artist_url))
      icon_url = nil
      # Use an xpath expression to tell Nokogiri what to look for.
      doc.xpath('//link[@rel="shortcut icon"]').each do |tag|
        taguri = URI(tag['href'])
        unless taguri.host.to_s.length < 1
          # There is a domain name in taguri, so we're good
          icon_url = taguri.to_s
        else
          # So we have to join it with the index URL we built at the beginning of the method
          icon_url = URI.join(artist_url, taguri).to_s
        end
      end
      respond_to do |format|
        format.html { redirect_to new_artist_path(
                                      name: artist_name,
                                      address: '',
                                      phone: artist_phone,
                                      website: artist_website,

                                      external_photo: artist_picture_url,
                                      external_icon: icon_url,

                                      trusted: artist_likes > 10000,
                                      enable_crawler: artist_likes > 10000,
                                      crawler_feed_url: artist_crawler,
                                  ), notice: 'Venue data imported.' }
      end
    end
  end

  private
  def buildFacebookVenue(url)
    @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
    #Then, get the facebook name
    fb_user = ICrawler.parse_user_id(url)
    raise "Cannot parse the facebook username: #{fb_user}" if !fb_user
    #Then, get the facebook ID and venue parameters
    fb_user_info = @graph.get_connections(fb_user, '')
    raise "Cannot get facebook user info for: #{fb_user}" if !fb_user_info

    venue_name = fb_user_info['name']

    #If there is no location defined, raise
    raise "fb page location doesn't exist" if !fb_user_info['location']

    venue_address = fb_user_info['location']['street']
    venue_phone = fb_user_info['phone']
    venue_website = 'http://' + (fb_user_info['website'].split(';').first)
    venue_latitude = fb_user_info['location']['latitude']
    venue_longitude = fb_user_info['location']['longitude']
    venue_likes = fb_user_info['likes']
    venue_crawler = "https://www.facebook.com/#{fb_user_info['id']}"

    #Lets find the country
    country = Country.find_country_by_name(fb_user_info['location']['country'])
    raise "Country not found: #{fb_user_info['location']['country']}" if !country
    country_alpha2 = country.alpha2

    #Lets find the city
    fb_city = fb_user_info['location']['city']
    cities = City.where(country_alpha2: country_alpha2).where("name = ? OR name_alt = ?", fb_city, fb_city)
    raise "City not found: #{fb_user_info['location']['country']} > #{fb_city}" if cities.empty?
    raise "Multiple cities found for: #{fb_user_info['location']['country']} > #{fb_city}" if cities.any? and cities.size > 1
    venue_city = cities.first

    #Lets get the profile picture with 200x200
    fb_user_picture = @graph.get_picture(fb_user_info['id'], {height: '200', width: '200'})
    raise "Cannot get facebook user picture for venue: #{fb_user}" if !fb_user_picture
    venue_picture_url = fb_user_picture

    #Lets get the website icon
    venue_url = venue_website
    doc = Nokogiri::HTML(open(venue_url))
    icon_url = nil
    # Use an xpath expression to tell Nokogiri what to look for.
    doc.xpath('//link[@rel="shortcut icon"]').each do |tag|
      taguri = URI(tag['href'])
      unless taguri.host.to_s.length < 1
        # There is a domain name in taguri, so we're good
        icon_url = taguri.to_s
      else
        # So we have to join it with the index URL we built at the beginning of the method
        icon_url = URI.join(venue_url, taguri).to_s
      end
    end
    respond_to do |format|
      format.html { redirect_to new_country_district_city_venue_path(
                                    venue_city.country_alpha2,
                                    venue_city.district_alpha2,
                                    venue_city.id,
                                    name: venue_name,
                                    address: venue_address,
                                    phone: venue_phone,
                                    website: venue_website,
                                    external_photo: venue_picture_url,
                                    external_icon: icon_url,
                                    latitude: venue_latitude,
                                    longitude: venue_longitude,
                                    trusted: venue_likes > 10000,
                                    enable_crawler: venue_likes > 10000,
                                    crawler_feed_url: venue_crawler,
                                ), notice: 'Venue data imported.' }
    end
    #Then, get the icon by parsing the page using Nokogiri
    #Then, fill the form of venue#new and redirect to complete the insertion

  end

=begin
      url = params[:url]
      @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
      #@fb_eventList = @graph.get_connections('TheatroCircoFanPage', 'events')
      raise 'Cannot find url parameter on the request' if(!url)
      #First, find the crawler class
      raise 'Cannot identify this crawler class' if ICrawler.get_crawler_class(url) != IVenueCrawler::FACEBOOK
      #Then, get the facebook name
      fb_user = ICrawler.parse_facebook_user_id(url)
      raise "Cannot parse the facebook username: #{fb_user}" if !fb_user
      p "Facebook user: #{fb_user}"
      #Then, get the facebook ID and venue parameters
      fb_user_info = @graph.get_connections(fb_user, '')
      raise "Cannot get facebook user info for: #{fb_user}" if !fb_user_info
      p "Facebook user info: #{fb_user_info['id']}"

      page_category_downcase = fb_user_info['category'].downcase
      if(IVenueCrawler::FACEBOOK_VENUE_CATEGORIES.include? page_category_downcase)
        venue_name = fb_user_info['name']

        #If there is no location defined, raise
        raise "fb page location doesn't exist" if !fb_user_info['location']

        venue_address = fb_user_info['location']['street']
        venue_phone = fb_user_info['phone']
        venue_website = 'http://' + (fb_user_info['website'].split(';').first)
        venue_latitude = fb_user_info['location']['latitude']
        venue_longitude = fb_user_info['location']['longitude']
        venue_likes = fb_user_info['likes']
        venue_crawler = "https://www.facebook.com/#{fb_user_info['id']}"

        #Lets find the country
        country = Country.find_country_by_name(fb_user_info['location']['country'])
        raise "Country not found: #{fb_user_info['location']['country']}" if !country
        country_alpha2 = country.alpha2

        #Lets find the city
        fb_city = fb_user_info['location']['city']
        cities = City.where(country_alpha2: country_alpha2).where("name = ? OR name_alt = ?", fb_city, fb_city)
        raise "City not found: #{fb_user_info['location']['country']} > #{fb_city}" if cities.empty?
        raise "Multiple cities found for: #{fb_user_info['location']['country']} > #{fb_city}" if cities.any? and cities.size > 1
        venue_city = cities.first

        #Lets get the profile picture with 200x200
        fb_user_picture = @graph.get_picture(fb_user_info['id'], {height: '200', width: '200'})
        p fb_user_picture
        raise "Cannot get facebook user picture for venue: #{fb_user}" if !fb_user_picture
        venue_picture_url = fb_user_picture

        #Lets get the website icon
        venue_url = venue_website
        doc = Nokogiri::HTML(open(venue_url))
        icon_url = nil
        # Use an xpath expression to tell Nokogiri what to look for.
        doc.xpath('//link[@rel="shortcut icon"]').each do |tag|
          taguri = URI(tag['href'])
          unless taguri.host.to_s.length < 1
            # There is a domain name in taguri, so we're good
            icon_url = taguri.to_s
          else
            # So we have to join it with the index URL we built at the beginning of the method
            icon_url = URI.join(venue_url, taguri).to_s
          end
        end
        respond_to do |format|
          format.html { redirect_to new_country_district_city_venue_path(
                                    venue_city.country_alpha2,
                                    venue_city.district_alpha2,
                                    venue_city.id,
                                      name: venue_name,
                                      address: venue_address,
                                      phone: venue_phone,
                                      website: venue_website,
                                      external_photo: venue_picture_url,
                                      external_icon: icon_url,
                                      latitude: venue_latitude,
                                      longitude: venue_longitude,
                                      trusted: venue_likes > 10000,
                                      enable_crawler: venue_likes > 10000,
                                      crawler_feed_url: venue_crawler,
                                    ), notice: 'Venue data imported.' }
          end
        elsif(IArtistCrawler::FACEBOOK_ARTIST_CATEGORIES.include? page_category_downcase)
          artist_name = fb_user_info['name']

          artist_phone = fb_user_info['phone']

          fb_user_website = fb_user_info['website'] || ''
          p 'fb_user_website:'
          p fb_user_website
          if fb_user_website.include? ';'
            artist_website = (fb_user_website.split(';').first)
          else #By default assume the space char as splitting the websites
            artist_website = (fb_user_website.split(' ').first)
          end
          p 'webiste:'
          p artist_website

          artist_website = ('http://' + artist_website) if !artist_website.blank? and !url_protocol_present?(artist_website)

      p "Facebook user id: #{fb_user_info['id']}"
      p "Artist website: #{artist_website}"
          artist_likes = fb_user_info['likes']
          artist_crawler = "https://www.facebook.com/#{fb_user_info['id']}"
      p "Artist crawler: #{artist_crawler}"

          #Lets get the profile picture with 200x200
          fb_user_picture = @graph.get_picture(fb_user_info['id'], {height: '200', width: '200'})
          p fb_user_picture
          raise "Cannot get facebook user picture for artist: #{fb_user}" if !fb_user_picture
          artist_picture_url = fb_user_picture

          #Lets get the website icon
          artist_url = artist_website
          doc = Nokogiri::HTML(open(artist_url))
          icon_url = nil
          # Use an xpath expression to tell Nokogiri what to look for.
          doc.xpath('//link[@rel="shortcut icon"]').each do |tag|
            taguri = URI(tag['href'])
            unless taguri.host.to_s.length < 1
              # There is a domain name in taguri, so we're good
              icon_url = taguri.to_s
            else
              # So we have to join it with the index URL we built at the beginning of the method
              icon_url = URI.join(artist_url, taguri).to_s
            end
          end
          respond_to do |format|
            format.html { redirect_to new_artist_path(
                                          name: artist_name,
                                          address: '',
                                          phone: artist_phone,
                                          website: artist_website,

                                          external_photo: artist_picture_url,
                                          external_icon: icon_url,

                                          trusted: artist_likes > 10000,
                                          enable_crawler: artist_likes > 10000,
                                          crawler_feed_url: artist_crawler,
                                      ), notice: 'Venue data imported.' }
          end
        else
          raise "Facebook page category doesn't include venue in category: #{fb_user_info['category']}; allowed venue list: #{IVenueCrawler::FACEBOOK_VENUE_CATEGORIES.to_sentence}; allowed artist list: #{IArtistCrawler::FACEBOOK_ARTIST_CATEGORIES.to_sentence}}"
      end
      #Then, get the icon by parsing the page using Nokogiri
      #Then, fill the form of venue#new and redirect to complete the insertion

    rescue Exception => e
      respond_to do |format|
        p e.backtrace
        flash[:alert] = "Could not import data:#{e.message}\nBacktrace:\n#{e.backtrace[0..5].join('\n')}"
        format.html { redirect_to crawlers_path }
      end
    end

  end
=end
end
