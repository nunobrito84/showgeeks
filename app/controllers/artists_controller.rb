class ArtistsController < ApplicationController

  load_and_authorize_resource :except => [:run] #This is necessary because it is not REST
  skip_load_resource :only => [:create] #This is necessary or it will fail

  # GET /artists
  # GET /artists.json
  def index
    @artists = Artist.all
  end

  # GET /artists/1
  # GET /artists/1.json
  def show
    require 'json'
    @latest_artist_crawler_reports = @artist.artist_crawler_reports.order(id: :desc)

    if ICrawler.get_crawler_class(@artist.crawler_feed_url) == ICrawler::FACEBOOK
      @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
      p "FB ID: #{ICrawler.parse_user_id(@artist.crawler_feed_url)}"
      @fb_eventList = @graph.get_connections(ICrawler.parse_user_id(@artist.crawler_feed_url), 'events')
    elsif ICrawler.get_crawler_class(@artist.crawler_feed_url) == ICrawler::BANDS_IN_TOWN
      @bandsintown_eventList = Bandsintown::Artist.new({:name => ICrawler.parse_bands_in_town_id(@artist.crawler_feed_url)}).events
    end

    @showList = @artist.shows.where("DATE(date) >= ?", Time.zone.now.beginning_of_day).order(date: :asc)
  end

  # GET /artists/new
  def new
    @artist = Artist.new(
                       name: params[:name],
                       address: params[:address],
                       phone: params[:phone],
                       website: params[:website],
                       external_photo: params[:external_photo],
                       external_icon: params[:external_icon],
                       trusted: params[:trusted],
                       enable_crawler: params[:enable_crawler],
                       crawler_feed_url: params[:crawler_feed_url]
    #external_icon: 'http://www.theatrocirco.com/favIcon.ico',
    #external_photo: "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/t1.0-1/p200x200/306132_10150920499747486_425451963_n.jpg"
    )
  end

  # GET /artists/1/edit
  def edit
  end

  # POST /artists
  # POST /artists.json
  def create
    @artist = Artist.new(artist_params)
    @artist.user_id = current_user.id

    respond_to do |format|
      if @artist.save
        format.html { redirect_to @artist, notice: 'Artist was successfully created.' }
        format.json { render action: 'show', status: :created, location: @artist }
      else
        format.html { render action: 'new' }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /artists/1
  # PATCH/PUT /artists/1.json
  def update
    respond_to do |format|
      if @artist.update(artist_params)
        format.html { redirect_to @artist, notice: 'Artist was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @artist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /artists/1
  # DELETE /artists/1.json
  def destroy
    @artist.destroy
    respond_to do |format|
      format.html { redirect_to artists_url }
      format.json { head :no_content }
    end
  end

  # GET artists/1/run
  include CrawlersHelper
  def run
    @artist = Artist.find(params[:artist_id])
    authorize! :run_artist_crawler, @artist # Cancan custom function

    if(@artist and ICrawler.get_crawler_class(@artist.crawler_feed_url))
      #Run rake task
      case ICrawler.get_crawler_class(@artist.crawler_feed_url)
        when IVenueCrawler::FACEBOOK
          job = FacebookArtistEvents.new(@artist)
        when IVenueCrawler::BANDS_IN_TOWN
          job = BandsInTownArtistEvents.new(@artist)
        else
          logStr = "Unknown artist crawler name for venue: #{@artist.name}"
      end
      if job
        initial_time = Time.now
        job.crawl
        logStr = crawlerLogAsHtmlList(job.log)
        job.set_duration(Time.at(Time.now.to_i - initial_time.to_i).utc)
        ArtistCrawlerReport.create(artist_id: @artist.id, duration: job.duration, success_count: job.success, edit_count: job.edit, cancelled_count: job.cancelled, fail_count: job.fail, skip_count: job.skip, log: logStr, automatic: false)
        redirect_to artist_path(@artist), notice: "Crawler #{@artist.name} complete."
      end
    else
      redirect_to artists_path, notice: "No artist or crawler found"
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def artist_params
      params.require(:artist).permit(:user_id, :visible, :subcategory_id, :name, :address, :country_alpha2, :phone, :website, :external_icon, :icon, :external_photo, :photo, :trusted, :enable_crawler, :crawler_feed_url)
    end
end
