class Users::Devise::RegistrationsController < Devise::RegistrationsController
  before_filter :resource_params

  protected
  def resource_params
    devise_parameter_sanitizer.for(:sign_up) << [:name, :avatar, :country_alpha2, :district_alpha2, :city_id, :affiliate_email]
    devise_parameter_sanitizer.for(:account_update) << []
  end
end
