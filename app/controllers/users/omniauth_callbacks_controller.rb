class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    handle_redirect('devise.facebook_data', 'Facebook')
  end

  private
  def handle_redirect(_session_variable, kind)
    # here we force the locale to the session locale so it siwtches to the correct locale
    I18n.locale = session[:omniauth_login_locale] || I18n.default_locale
    user = User.from_omniauth(request.env["omniauth.auth"])
    if user.persisted?
      sign_in_and_redirect user, event: :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => kind) if is_navigational_format?
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

=begin
  #def twitter
  #  handle_redirect('devise.twitter_uid', 'Twitter')
  #end
  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
=end
end