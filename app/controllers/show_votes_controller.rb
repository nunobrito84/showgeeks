class ShowVotesController < ApplicationController
  load_and_authorize_resource

  # GET /show_votes
  # GET /show_votes.json
  def index
    @show_votes = ShowVote.all
  end

  # POST /show_votes
  # POST /show_votes.json
  def create
    success = false
    if(params[:show_vote])
      @show_vote = ShowVote.new(show_vote_params)
      success = @show_vote.save
    else
      @show_vote = ShowVote.find_by(user_id: current_user.id, show_id: params[:show_id], vote_type: params[:vote_type])
      if !@show_vote
        @show_vote = ShowVote.new(user_id: current_user.id, show_id: params[:show_id], vote_type: params[:vote_type], amount: 1)
        success = @show_vote.save
      end
    end

    #Find show
    if success
      @show = @show_vote.show
      if(@show)
        peopleAttendingCount = @show.show_votes.attending.count
        if (@show.attending != peopleAttendingCount)
          @show.update_attributes(attending: peopleAttendingCount)
        end
      end
    end

    #Reply as js
    respond_to do |format|
      if success
        format.html { redirect_to show_votes_path, notice: 'Operation successful.' }
        format.js { }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.js
        format.json { render json: @show_vote.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /show_votes/1
  # DELETE /show_votes/1.json
  def destroy
    success = false
    @show_vote = current_user.show_votes.find(params[:id])
    if @show_vote
      success = @show_vote.destroy
    end
    if success
      #Find show
      @show = @show_vote.show
      if(@show)
        peopleAttendingCount = @show.show_votes.attending.count
        if (@show.attending != peopleAttendingCount)
          @show.update_attributes(attending: peopleAttendingCount)
        end
      end
    end
    respond_to do |format|
      format.html { redirect_to show_votes_url }
      format.js
      format.json { head :no_content }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def show_vote_params
      params.require(:show_vote).permit(:user_id, :show_id, :vote_type, :amount, :info)
    end
end
