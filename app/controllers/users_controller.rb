class UsersController < ApplicationController
  load_and_authorize_resource

  # GET /users
  def index
    if current_user.admin
      @users = User.all
    else
      @users = current_user.clients
    end
  end

  # GET /users/1
  def show
    #p @user.inspect
  end

  # GET /users/1/edit
  def edit
  end

  # PATCH/PUT /users/1
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to user_path(@user), notice: 'User was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path() }
    end
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    if can? :edit_user_class, User
      params.require(:user).permit(:name, :is_seller, :affiliate_email, :country_alpha2, :district_alpha2, :city_id, :avatar)
    else
      params.require(:user).permit(:name, :country_alpha2, :district_alpha2, :city_id, :avatar)
    end
  end

end