class ShowsController < ApplicationController
  include ApplicationHelper
  #before_action :set_show, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:filters_state, :set_visible]
  skip_load_resource :only => :create

  private
  def fetch_shows_list
    #Set session parameters
    session[:country] = params[:country] || requestCountry
    session[:district] = params[:district] unless params[:district].nil?
    session[:category_id] = params[:category_id] # || Category::DEFAULT_CATEGORY
    session[:show_all_shows] = params[:show_all_shows] unless params[:show_all_shows].nil?

    #session[:show_all_shows] = false

    #p "session[:country]:#{session[:country]}"
    #p "session[:district]:#{session[:district]}"
    #p "session[:category_id]:#{session[:category_id]}"

    country = session[:country]
    district = session[:district]
    category_id = session[:category_id]

    @showList = Show.where("DATE(date) >= ?", Time.zone.now.beginning_of_day).order(city_name: :asc).order(trust: :desc).order(date: :asc).page(params[:page]).per_page(30)
    if !country.blank?
      #Apply country
      @showList = @showList.from_country(country)
      #Apply district
      @showList = @showList.from_district(district) if !district.blank?
      puts "Cnt: #{@showList.count} district: #{district} country: #{country}"
    end
    #Apply category
    if category_id
      @showList = @showList.where(category_id: category_id)
    end
    #Apply visible
    if !user_signed_in? or !current_user.admin?
      @showList = @showList.where(visible: Show::VISIBLE_YES)
    else #admin options
      @showList = @showList.where("visible > ?", Show::VISIBLE_IGNORE)
      @decision_pending_count = Show.where(visible: Show::VISIBLE_DONT_KNOW).count

      session[:view_undecided] = params[:view_undecided] unless params[:view_undecided].nil?
      #p "Session[:view_undecided]: #{session[:view_undecided]}"
      @view_undecided_option = session[:view_undecided] == 'true'
      if @view_undecided_option
        @showList = @showList.where(visible: Show::VISIBLE_DONT_KNOW)
      end
    end

    @showList = @showList || []

  end

  public
  # GET /shows
  # GET /shows.json
  def index
    #@showLength = Show.count
    @showList = fetch_shows_list
    @showLength = @showList.size
    @venues_list = Venue.all
    @remoteCountry = requestCountry()
  end

  def reload_shows
    @showList = fetch_shows_list
    respond_to do |format|
      format.js
    end
  end

  def reload_suggestion
    @country = params[:country]
    @district_name = Country[params[:country]].states[params[:district]]['name']
    @district_alpha2 = params[:district]
    respond_to do |format|
      format.js
    end
  end

  # GET /shows/1
  # GET /shows/1.json
  def show
  end

  # GET /shows/new
  def new
    @show = Show.new(country_alpha2: current_user.country_alpha2,
                     district_alpha2: current_user.district_alpha2,
                     city_id: current_user.city_id)
  end

  # GET /shows/1/edit
  def edit
  end

  # POST /shows
  # POST /shows.json
  def create
    @show = Show.new(show_params)
    @show.user = current_user
    respond_to do |format|
      if @show.save
        format.html { redirect_to @show, notice: 'Show was successfully created.' }
        format.json { render action: 'show', status: :created, location: @show }
      else
        format.html { render action: 'new' }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
    p @show.errors.inspect
  end

  # PATCH/PUT /shows/1
  # PATCH/PUT /shows/1.json
  def update
    respond_to do |format|
      if @show.update(show_params)
        format.html { redirect_to @show, notice: 'Show was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shows/:show_id/set_visible
  # PATCH/PUT /shows/:show_id/set_visible.json
  def set_visible
    @show = Show.find(params[:show_id])
    authorize! :set_visible, @show # Cancan custom function

    @show.update(show_params)
    respond_to do |format|
     format.js
    end
  end

  # DELETE /shows/1
  # DELETE /shows/1.json
  def destroy
    @show.destroy
    respond_to do |format|
      format.html { redirect_to shows_url }
      format.json { head :no_content }
    end
  end

  #GET filters_state
  #This function is requested by the JS map to synchronize with the selected options
  def filters_state
    respond_to do |format|
      format.json { }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_show
      @show = Show.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def show_params
      params.require(:show).permit(:visible,
                                   :name,
                                   :date,
                                   :country_alpha2, :district_alpha2, :city_id, :city_name,
                                   :venue_id, :venue_name,
                                   :artist_id,
                                   :description, :photo_url, :price, :photo)
    end
end
