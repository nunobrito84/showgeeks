class VenuesController < ApplicationController
  before_action :set_city

  load_and_authorize_resource :except => [:run] #This is necessary because it is not REST
  skip_load_resource :only => [:create] #This is necessary or it will fail

  # GET /venues
  # GET /venues.json
  def index
    @venues = Venue.from_city(params[:city_id])
  end

  # GET /venues/1
  # GET /venues/1.json
  def show
    require 'json'
    @latest_venue_crawler_reports = @venue.venue_crawler_reports.order(id: :desc)

    if IVenueCrawler.get_crawler_class(@venue.crawler_feed_url) == IVenueCrawler::FACEBOOK
      @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
      @fb_eventList = @graph.get_connections(ICrawler.parse_user_id(@venue.crawler_feed_url), 'events')
    end

    @showList = @venue.shows.where("DATE(date) >= ?", Time.zone.now.beginning_of_day).order(date: :asc)
  end

  # GET /venues/new
  def new
    @venue = Venue.new(city_id: @city.id,
                       name: params[:name],
                       address: params[:address],
                       phone: params[:phone],
                       website: params[:website],
                       external_photo: params[:external_photo],
                       external_icon: params[:external_icon],
                       latitude: params[:latitude],
                       longitude: params[:longitude],
                       trusted: params[:trusted],
                       enable_crawler: params[:enable_crawler],
                       crawler_feed_url: params[:crawler_feed_url]
                       #external_icon: 'http://www.theatrocirco.com/favIcon.ico',
                       #external_photo: "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/t1.0-1/p200x200/306132_10150920499747486_425451963_n.jpg"
                      )
  end

  # GET /venues/1/edit
  def edit
  end

  # POST /venues
  # POST /venues.json
  def create
    @venue = Venue.new(venue_params)
    @venue.user_id = current_user.id
    respond_to do |format|
      if @venue.save
        format.html { redirect_to country_district_city_venue_path(@venue.city.country_alpha2, @venue.city.district_alpha2, @venue.city_id, @venue), notice: 'Venue was successfully created.' }
      else
        p @venue.errors.inspect
        format.html { render action: 'new' }
      end
    end
  end

  # PATCH/PUT /venues/1
  # PATCH/PUT /venues/1.json
  def update
    respond_to do |format|
      if @venue.update(venue_params)
        format.html { redirect_to country_district_city_venue_path(@venue.city.country_alpha2, @venue.city.district_alpha2, @venue.city_id, @venue), notice: 'Venue was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /venues/1
  # DELETE /venues/1.json
  def destroy
    country_alpha2 = @venue.city.country_alpha2
    district_alpha2 = @venue.city.district_alpha2
    city_id = @venue.city_id
    @venue.destroy
    respond_to do |format|
      format.html { redirect_to country_district_city_venues_path(country_alpha2, district_alpha2, city_id) }
    end
  end

  # GET venues/1/run
  include CrawlersHelper
  def run
    @venue = Venue.find(params[:venue_id])
    authorize! :run_venue_crawler, @venue # Cancan custom function

    if(@venue and IVenueCrawler.get_crawler_class(@venue.crawler_feed_url))
      #Run rake task
      case IVenueCrawler.get_crawler_class(@venue.crawler_feed_url)
        when IVenueCrawler::YEAAAAH
          job = Yeaaaah.new(@venue)
        when IVenueCrawler::FACEBOOK
          job = FacebookVenueEvents.new(@venue)
        else
          logStr = "Unknown venue crawler name for venue: #{@venue.name}"
      end
      if job
        initial_time = Time.now
        job.crawl
        logStr = crawlerLogAsHtmlList(job.log)
        job.set_duration(Time.at(Time.now.to_i - initial_time.to_i).utc)
        VenueCrawlerReport.create(venue_id: @venue.id, duration: job.duration, success_count: job.success, edit_count: job.edit, cancelled_count: job.cancelled, fail_count: job.fail, skip_count: job.skip, log: logStr, automatic: false)
        redirect_to country_district_city_venue_path(@city.country_alpha2, @city.district_alpha2, @city.id, @venue.id), notice: "Crawler #{@venue.name} complete."
      end
    else
      redirect_to country_district_city_venue_path(@city.country_alpha2, @city.district_alpha2, @city, @venue), notice: "No venue or crawler found"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def venue_params
      params.require(:venue).permit(:user, :visible, :name, :address, :phone, :city_id, :website,
                                    :external_icon, :icon,
                                    :external_photo, :photo,
                                    :latitude, :longitude, :trusted, :enable_crawler, :crawler_feed_url)
    end
end
