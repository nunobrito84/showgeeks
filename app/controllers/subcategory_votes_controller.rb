class SubcategoryVotesController < ApplicationController
  load_and_authorize_resource

  # GET /subcategory_votes
  # GET /subcategory_votes.json
  def index
    @subcategory_vote = SubcategoryVote.all
  end

  # POST /subcategory_votes
  # POST /subcategory_votes.json
  def create
    @subcategory_vote = SubcategoryVote.find_by(user_id: current_user.id, show_id: params[:show_id])
    if @subcategory_vote
      #remove vote
      if @subcategory_vote.subcategory_id == params[:subcategory_id].to_i
        @subcategory_vote.destroy
      else #edit vote... change subcategory_id
        @subcategory_vote.update_attributes(:subcategory_id => params[:subcategory_id])
      end
    else #create object
       @new_vote = SubcategoryVote.new(user_id: current_user.id, show_id: params[:show_id], subcategory_id: params[:subcategory_id], amount: current_user.trust)
       @new_vote.save
    end
    #Update the no-normalized show column
    @show = Show.find(params[:show_id])
    if(@show)
      @favouriteCategory = @show.subcategory_votes.joins(:subcategory).select('category_id, subcategory_id, sum(amount) as value').group('subcategory_id').order('value DESC').limit(1).first
      if @favouriteCategory != nil
        if (@show.category_id != @favouriteCategory.category_id) || (@show.subcategory_id != @favouriteCategory.subcategory_id)
          @show.update_attributes(category_id: @favouriteCategory.category_id, subcategory_id: @favouriteCategory.subcategory_id)
        end
      else
        @show.update_attributes(category_id: nil, subcategory_id: nil)
      end
    end
    #Reply as js
    respond_to do |format|
      format.js
    end
  end

end
