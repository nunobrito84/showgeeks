class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if @contact.deliver
      redirect_to root_path, flash: {notice: I18n.t('contacts.contact.message_send_success') }
    else
      flash.now[:error] = I18n.t('contacts.contact.message_send_error')
      render :new
    end

  end
end