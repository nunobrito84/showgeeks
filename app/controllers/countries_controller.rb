class CountriesController < ApplicationController
  include CountriesHelper
  before_action :set_country, only: [:show]
  load_and_authorize_resource :class => false, :except => [:districts_select] #Because there is no model for this class

  # GET /countries
  # GET /countries.json
  def index
    @countries = available_countries
  end

  # GET /countries/1
  # GET /countries/1.json
  def show
  end

  # GET /countries/US/districts_select.js
  def districts_select
    respond_to do |format|
      format.js
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_country
    @country = Country[params[:id]]
  end
end
