class DistrictsController < ApplicationController
  before_action :set_country
  before_action :set_district, only: [:show]

  load_and_authorize_resource :class => false #Because there is no model for this class

  # GET /districts
  # GET /districts.json
  def index
    @districts = @country.states.select {|n| !['AA','AE','AP','AS','GU','MP','PR','UM','VI'].include? n}
  end

  # GET /districts/1
  # GET /districts/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_district
      dist_obj = @country.states.select {|n| [params[:id]].include? n}.first
      @district = { alpha2: dist_obj[0], name: dist_obj[1]['name'] }
    end
    def set_country
      @country = Country[params[:country_id]]
    end

end
