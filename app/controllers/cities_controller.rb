class CitiesController < ApplicationController
  before_action :set_country_district
  load_and_authorize_resource
  skip_load_resource only: :create
  #before_action :set_city, only: [:show, :edit, :update, :destroy]

  # GET /cities
  # GET /cities.json
  def index
    @cities = City.where(country_alpha2: @country.alpha2, district_alpha2: @district[:alpha2]) || []
  end

  # GET /cities/1
  # GET /cities/1.json
  def show
  end

  # GET /cities/new
  def new
    @city = City.new(country_alpha2: @country.alpha2, district_alpha2: @district[:alpha2])
  end

  # GET /cities/1/edit
  def edit
  end

  # POST /cities
  # POST /cities.json
  def create
    @city = City.new(city_params)
    respond_to do |format|
      if @city.save
        format.html { redirect_to country_district_city_path(@country.alpha2, @district[:alpha2], @city), notice: 'City was successfully created.' }
        format.json { render action: 'show', status: :created, location: @city }
      else
        format.html { render action: 'new' }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cities/1
  # PATCH/PUT /cities/1.json
  def update
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to country_district_city_path(@country.alpha2, @district[:alpha2], @city), notice: 'City was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to country_district_cities_path(@country.alpha2, @district[:alpha2]) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
   def set_country_district
      @country = Country[params[:country_id]]
      dist_obj = @country.states.select {|n| [params[:district_id]].include? n}.first
      @district = { alpha2: dist_obj[0], name: dist_obj[1]['name'] }
    end

  # Never trust parameters from the scary internet, only allow the white list through.
    def city_params
      params.require(:city).permit(:country_alpha2, :district_alpha2, :name, :name_alt)
    end
end
