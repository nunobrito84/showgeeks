class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  protect_from_forgery with: :exception
  #check_authorization

  before_filter :set_i18n_locale_from_params
  before_filter :get_categories
  before_filter :set_locale

  def default_url_options(options={})
    { :locale => I18n.locale }
  end

  #I must change the way this is done, but later...
  def get_categories
    @categories = Category.all
  end

  def contacts
    render 'layouts/public/contacts'
  end

  def privacy
    render 'layouts/public/privacy'
  end

  def terms
    render 'layouts/public/terms'
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  protected
  def set_i18n_locale_from_params
    if params[:locale]
      if I18n.available_locales.include?(params[:locale].to_sym)
        I18n.locale = params[:locale]
      else
        flash.now[:notice] ="#{params[:locale]} translation not available"
        logger.error flash.now[:notice]
      end
    end
  end

  private
  def set_locale
    I18n.locale = params[:locale] || extract_locale_from_accept_language_header || I18n.default_locale
  end

  private
  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

end
