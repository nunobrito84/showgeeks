class CategoriesController < ApplicationController
  load_and_authorize_resource

  # GET /categories
  def index
    @categories = Category.all
  end

  def show
  end

end
