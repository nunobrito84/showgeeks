ready = ->  #Turbolinks compatible

  # Datetime picker
  $('[data-behaviour~=datetimepicker]').datetimepicker({
    pickDate: true,
    pickTime: true,
    showSecond: true,
    dateFormat: 'YYYY-MM-DD hh:mm:ss',
    language: navigator.language || navigator.userLanguage #This detects the PC language
  });

  # Datetime picker
  $('[data-behaviour~=datepicker]').datetimepicker({
    pickDate: true,
    pickTime: false,
    format: 'YYYY-MM-DD'
  #format: 'DD/MM/YYYY'
  });

  # Endless index load
  if $('.pagination').length
    $(window).scroll ->
      url = $('.pagination .next_page').attr('href')
      if url && $(window).scrollTop() > $(document).height() - $(window).height() - 50
        $('.pagination').text("loading more events ...")
        $.getScript(url)
    $(window).scroll()

  # Date picker
  #$('[data-behaviour~=datepicker]').datepicker(
  #  format: 'dd-mm-yyyy',
  #  todayBtn: 'linked',
  #  language: 'pt-BR')

  # On Country change
  $('#cb_generic_country').change (element) ->
    $('#cb_generic_district').empty()
    $('#cb_generic_city').empty()
    if(this.value.length > 0)
      $.getJSON "/countries/#{this.value}/districts", (data) ->
        $.each(data, (i, item) ->
          $('#cb_generic_district').append($('<option></option>').val(item.alpha2).html(item.name));
          return)
        return
    else #If the selection is empty, include an empty value, otherwise, the values won't change in the database
      $('#cb_generic_district').append($('<option></option>'));
      $('#cb_generic_city').append($('<option></option>'));

  # On District change
  $('#cb_generic_district').change (element) ->
    $('#cb_generic_city').empty()
    $('#cb_generic_city').val([]) #unselect
    cb_generic_country = $('#cb_generic_country')[0]
    selected_country_code = cb_generic_country[cb_generic_country.selectedIndex].value
    $.getJSON "/countries/#{selected_country_code}/districts/#{this.value}/cities", (data) ->
    #$.get "/maps/states/#{$('#cb_generic_country')[0].value}/cities/#{element.target.value}.json", (data) ->
      $.each(data, (i, item) ->
        $('#cb_generic_city').append($('<option></option>').val(item.id).html(item.name));
        return)
      return

  # On City change
  $('#cb_generic_city').change (element) ->
    $('#cb_generic_venue').empty()
    $('#cb_generic_venue').val([]) #unselect
    cb_generic_country = $('#cb_generic_country')[0]
    selected_country_code = cb_generic_country[cb_generic_country.selectedIndex].value

    cb_generic_district = $('#cb_generic_district')[0]
    selected_district_code = cb_generic_district[cb_generic_district.selectedIndex].value

    $.getJSON "/countries/#{selected_country_code}/districts/#{selected_district_code}/cities/#{this.value}/venues", (data) ->
      $.each(data, (i, item) ->
        $('#cb_generic_venue').append($('<option></option>').val(item.id).html(item.name));
        return)
      return
  return

#Turbolinks compatible
$(document).ready(ready)
$(document).on('page:load', ready)