ready = -> #Turbolinks compatible

  fill_districts = (data)->
    $('#cb_district').unbind() # Remove events that may have for GC to do its job
    html = ""
    if data.length > 0
      html += "<option value=''>all</option>"
    for obj in data
      html += "<option value=#{obj.alpha2}>#{obj.name}</option>"
    $('#cb_district').html html
    $('#cb_district').val ''

  # On Country change
  $('#cb_country').change (element, defaultCityCode, showSuggestion) ->
    #$('#div_showList').html('')
    selected_country_code = this[this.selectedIndex].value
    CreateMap(selected_country_code)

    #ia aki a ajustar este mecanismo
    $.ajax "/countries/#{this.value}/districts_select",
      type: 'GET'
      dataType: 'script'
      data: {
        country_alpha2: this.value
      }
      error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
    ###
    $.getJSON "/countries/#{this.value}/districts", (data) ->
      fill_districts(data)

      if defaultCityCode
        # Lets find the city where the user is from
        cb_country = $('#cb_country')[0]
        if showSuggestion
          cb_district = $('#cb_district')[0]
          for i in [0..cb_district.options.length] by 1
            if cb_district.options[i].value == defaultCityCode
              $.post '/reload_suggestion', {
                country: cb_country.options[cb_country.selectedIndex].value
                district: cb_district.options[i].value
              }
              break
        else
          $('#cb_district').val(defaultCityCode)
          $('#cb_district').trigger("change")
          $('#country_map').js_map('selectRegion', defaultCityCode)

      # On District State change
      $('#cb_district').change (element) ->
        $('#country_map').js_map('selectRegion', element.target.value)
        #console.error(element.target.options[element.target.selectedIndex].text)
        cb_country = $('#cb_country')[0]
        country = cb_country.options[cb_country.selectedIndex].value
        #console.error(element.target.options[element.target.selectedIndex].text)
        district = null
        if(element.target.selectedIndex > 0)
          district = element.target.options[element.target.selectedIndex].value
        #console.error()
        $.post '/reload_shows', {
          country: country
          district: district
        }

        return
      return
    return   ###

  $('#slider').slider(
    animate: true,
    max: 100,
    value: 50,
    change: ( event, ui ) ->
      $('#amount').text("Distance: #{ui.value} km"))

  $('#city').append "Loading info..."

  #Get session data to be coherent when refreshing or returnin to main page
  $.getJSON "/filters_state", (data) ->
    if data.country != null && data.district != null
      #We have session location... apply it...
      $('#cb_country').val(data.country)
      $('#cb_country').trigger("change", [data.district, false])
      $('#clicked-state').text('You clicked: ' + data.country +  ' code: ' + data.district)
    else
      #$.get 'http://freegeoip.net/json/193.136.128.69' #This is from IST in Lisbon
      $.get('http://freegeoip.net/json/')
      #.error (response) ->
      #$('#cb_country').trigger("change")
      .success (location) ->
          #$.get 'http://freegeoip.net/json/193.136.128.69', (location) -> #This is from IST in Lisbon
          location.region_code = FIPS_to_ISO(location.country_code, location.region_code)
          #location.country_code = 'US'
          #location.country_code = 'BR'
          #console.info('I am faking the district')
          #location.region_code = '03' #Fake as Porto
          #Put the country list with the right country
          #cb_c = $('#cb_country')
          $('#cb_country').val(location.country_code)
          $('#cb_country').trigger("change", [location.region_code, true])
          #$('#clicked-state').text('You clicked: ' + location.country_code +  ' code: ' + location.region_code)
      #CreateMap(location.country_code)


CreateMap = (country_code) ->
  $('#country_map').empty() #Remove any previous map that may be used
  $('#country_map').js_map
    stateStyles: (fill: 'peru')
    showLabels: true
    stateHoverStyles: (fill: 'lightyellow')
    country_code: country_code
    click: (event, data) ->
      $('#clicked-state').text('You clicked: ' + data.name +  ' code: ' + data.code)
      $('#cb_district').val(data.code)
      $.post '/reload_shows', {
        country: country_code
        district: data.code
      }
      return

FIPS_to_ISO = (country_code, value) ->
  switch country_code
    when 'PT'
      #Conversion matrix between FIPS and ISO_3166-2:pt
      pairs=[['02', '01'],
             ['03', '02'],
             ['04', '03'],
             ['05', '04'],
             ['06', '05'],
             ['07', '06'],
             ['08', '07'],
             ['09', '08'],
             ['10', '30'],
             ['11', '09'],
             ['13', '10'],
             ['14', '11'],
             ['16', '12'],
             ['17', '13'],
             ['18', '14'],
             ['19', '15'],
             ['20', '16'],
             ['21', '17'],
             ['22', '18'],
             ['23', '20']]
      return pair[1] for pair in pairs when pair[0] == value

#Turbolinks compatible
$(document).ready(ready)
$(document).on('page:load', ready)