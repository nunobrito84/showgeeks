module CrawlersHelper

  def crawlerLogAsHtmlList(logArray)
    logArray.sort! { |a,b| a[:level] <=> b[:level] } #Sort by priority levels
    logStr = '<ul>'
    logArray.each do |log_item|
      case log_item[:level]
        when IVenueCrawler::INFO
          logStr.concat "<li><p class='text-info'>#{log_item[:message]}</p></li>"
        when IVenueCrawler::LIGHT_ERROR
          logStr.concat "<li><p class='text-warning'>#{log_item[:message]}</p></li>"
        when IVenueCrawler::SEVERE_ERROR
          logStr.concat "<li><p class='text-danger'>#{log_item[:message]}</p></li>"
        else
          logStr.concat "<li><p class=''>#{log_item[:message]}</p></li>"
      end
    end
    logStr.concat '</ul>'
    return logStr
  end

  def url_protocol_present?(url)
    url[/\Ahttp:\/\//] || url[/\Ahttps:\/\//]
  end

end
