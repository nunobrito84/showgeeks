module CountriesHelper

  def available_countries
    [] << Country['US'] << Country['PT'] << Country['BR']
  end

  def show_state(country_alpha2)
    case country_alpha2
      when 'US'
        true
      else
        false
    end
  end

end
