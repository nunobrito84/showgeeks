module UsersHelper

  def user_type_icon(user)
    if user.admin
      "<i class='glyphicon glyphicon-tower'></i>"
    elsif user.is_seller
      "<i class='glyphicon glyphicon-briefcase'></i>"
    else
      "<i class='glyphicon glyphicon-user'></i>"
    end
  end

end
