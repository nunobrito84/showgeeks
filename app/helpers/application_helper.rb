module ApplicationHelper

  MONTHS_PT = %w(Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro)
  def options_from_collection_for_select_with_attributes(collection, value_method, text_method, attr_name, attr_field, selected = nil)
    options = collection.map do |element|
      [element.send(text_method), element.send(value_method), attr_name => element.send(attr_field)]
    end

    selected, disabled = extract_selected_and_disabled(selected)
    select_deselect = {}
    select_deselect[:selected] = extract_values_from_collection(collection, value_method, selected)
    select_deselect[:disabled] = extract_values_from_collection(collection, value_method, disabled)

    options_for_select(options, select_deselect)
  end

  public
  def requestCountry
    @geoip ||= GeoIP.new("#{Rails.root}/db/GeoIP.dat")
    remote_ip = request.remote_ip if request.remote_ip != "127.0.0.1" #todo: check for other local addresses or set default value
    location_location = @geoip.country(remote_ip)
    if location_location != nil
      if !Country.find_by_alpha2(location_location['country_code2']).nil? and available_countries.any? {|c| c.alpha2 == location_location['country_code2']}
        return location_location['country_code2']
      end
    end
    return 'US' #By default use US
  end
end
