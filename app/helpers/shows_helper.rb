module ShowsHelper

  def get_unique_show_venues
    Venue.pluck(:id, :name)
  end

  def favouriteCategoriesList
    @show.subcategory_votes.joins(:subcategory).select('name, subcategory_id, sum(amount) as value').group('subcategory_id').order('value DESC').limit(4)
  end

  def remainingCategoriesList(exceptList)
    @categories_subcategories = Subcategory.select('categories.id as category_id, categories.name as category_name, subcategories.id as subcategory_id, subcategories.name as subcategory_name').joins('LEFT OUTER JOIN categories ON subcategories.category_id = categories.id').order('category_name ASC', 'subcategory_name ASC')
    #Now remove the favourite ones from the complete list
    exceptList.each do |favouriteCat|
      @categories_subcategories.each_with_index do |cat_item, i|
        if favouriteCat.subcategory_id == cat_item.subcategory_id
          puts "removing favourites #{cat_item.subcategory_name} from category list"
          @categories_subcategories.to_a.delete_at(i)
          break
        end
      end
    end
    @categories_subcategories
  end

  def complaintsList
    complaintsList = @show.show_votes.where(vote_type: [ShowVote::TYPE_COMPLAINT_WRONG_INFO, ShowVote::TYPE_COMPLAINT_SPAM, ShowVote::TYPE_COMPLAINT_ABUSE ]).select('vote_type, sum(amount) as count').group('vote_type').order('count DESC')
    complaintsList.to_a.delete_if do |complaint|
      if(complaint['count'] < ShowVote::COMPLAINT_SHOW_THRESHOLD)
        true
      end
    end
  end

  def attending_badge(show)
    if show.attending >= ShowVote::GREEN_EVENT_ATTENDANCE_LEVEL
      'badge-success'
    elsif show.attending >= ShowVote::BLUE_EVENT_ATTENDANCE_LEVEL
      'badge-info'
    else
      ''
    end
  end

  def show_line_class(show)
    case show.visible
      when Show::VISIBLE_DONT_KNOW
        'alert-danger'
      when Show::VISIBLE_NO
        'alert-info'
    end
  end

  def show_untrusted_shows
    session[:show_all_shows] || false
  end

end
