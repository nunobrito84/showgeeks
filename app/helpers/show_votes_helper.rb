module ShowVotesHelper

  def markedAsWrongInfo?
    return current_user.show_votes.complaining_wrong_info.this_show(@show.id).count > 0
  end
  def markedAsSpam?
    return current_user.show_votes.complaining_spam.this_show(@show.id).count > 0
  end
  def markedAsAbusive?
    return current_user.show_votes.complaining_abuse.this_show(@show.id).count > 0
  end

end
