class ShowCrawlerNotifier < ActionMailer::Base
  default from: 'Showgeeks Admin<admin@showgeeks.com>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.show_crawler_notifier.processed.subject
  #
  def processed(artistJobList, venueJobList)
    @artistJobList = artistJobList
    @venueJobList = venueJobList

    @durationCount = Time.at(0)
    @successCount = 0
    @editCount = 0
    @cancelledCount = 0
    @failCount = 0
    @skipCount = 0

    # artists
    @artistJobList.each do |job|
      p job.duration
      @durationCount = Time.at(@durationCount.to_i + job.duration.to_i)
      @successCount += job.success
      @editCount += job.edit
      @cancelledCount += job.cancelled
      @failCount += job.fail
      @skipCount += job.skip
    end
    # venues
    @venueJobList.each do |job|
      @durationCount = Time.at(@durationCount.to_i + job.duration.to_i)
      @successCount += job.success
      @editCount += job.edit
      @cancelledCount += job.cancelled
      @failCount += job.fail
      @skipCount += job.skip
    end

    emailTitle = 'Showgeeks rpt:'
    emailTitle.concat(" #{@durationCount.utc.strftime("%H:%M:%S")} [suc:#{@successCount} | edt:#{@editCount} | canc:#{@cancelledCount} | fail:#{@failCount} | skip: #{@skipCount}]")
    mail to: 'nunobrito84@gmail.com', subject: emailTitle, :content_type => "text/html"
  end
end
