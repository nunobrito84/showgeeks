Showgeeks::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = { :host => 'showgeeks.com' }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      address: 'showgeeks.com',
      port: 587,
      domain: 'showgeeks.com',
      authentication: 'plain',
      user_name: 'development.showgeeks',
      password: 'developer',
      enable_starttls_auto: true,
      openssl_verify_mode: 'none'
  }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Simulate IP for the geocoder
  #require 'spoof_ip'
  #config.middleware.use('SpoofIp', '64.71.24.19') #US San Francisco, California
  #config.middleware.use('SpoofIp', '193.137.16.117') #PT Guimaraes
  #config.middleware.use('SpoofIp', '63.250.180.32') #Brazil, Rio de Janeiro
  #config.middleware.use('SpoofIp', '177.21.208.20') #Brazil, Rio grande do sul, VERANOPOLIS
  #config.middleware.use('SpoofIp', '177.36.32.11') #Brazil, Minas Gerais, Governador Valadares

  #Paperclip
  Paperclip.options[:command_path] = "/usr/bin/"

end
