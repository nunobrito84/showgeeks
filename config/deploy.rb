require "bundler/capistrano"
require "whenever/capistrano"

set :user, 'farmaco' # The SSH username you are logging into the server(s) as. Using SSH Keys is recommended.
set :password, 'fcosuper' # The SSH password. This is optional. By default password will be prompted. Using SSH Keys is recommended.
set :scm_password, #proc{Capistrano::CLI.password_prompt('Bitbucket password:' )}

set :use_sudo, false # If the user have sudo access, you can set this to true.
set :application, "showgeeks" # Your application name
set :deploy_to, "/home/#{user}/#{application}" # Path in the server where the app will be deployed
set :repository,  "git@bitbucket.org:nunobrito84/Showgeeks.git" # Your svn checkout or git clone url

set :scm, :git # specify your version control system (subversion, mercurial etc) 
set :bundle_dir, "/home/#{user}/#{application}/shared/bundle"  # Gems bundle dir

set :normalize_asset_timestamps, false # if you're using asset pipeline, set this to false#
default_run_options[:pty] = true  # if you get stdin: is not a tty error, uncomment it

role :web, 'showgeeks.com'                   # Your HTTP server, Apache/etc
role :app, 'showgeeks.com'                   # This may be the same as your `Web` server
role :db,  'showgeeks.com', :primary => true # This is where Rails migrations will run

# if you want to clean up old releases on each deploy uncomment this:
# set :keep_releases, 5 
after "deploy:restart", "deploy:cleanup"
set :ssh_options, { :forward_agent => true } #This is essential for the SSH to work between servers

#If you want to change the ownership and permissions after deploy uncomment the task below
after "deploy:update_code" do
  run "mkdir -p #{deploy_to}/shared/system" #create assets shared dir if not exists
  run "chmod 755 #{release_path} -R"
  run "chown -R #{user}:#{user} #{release_path}"
  #Important: lets precompile the assets!l
  run "cd #{release_path}; bundle exec rake assets:precompile RAILS_ENV=#{rails_env}"
end

#Config Cron Jobs
after "deploy:create_symlink", "deploy:update_crontab"
namespace :deploy do
  desc "Update the crontab file"
  task :update_crontab, :roles => :db do
    #Remove old cron entry and apply the new one
    #run "cd #{current_path} && crontab -r -u #{application} && whenever --update-crontab #{application}"
    run "cd #{current_path} && whenever --update-crontab #{application}"
  end
end

namespace :deploy do
  #run "cd #{current_path}"
  #run "bundle install --path='/home/farmados/showgeeks/shared/bundle'"
  desc "cold deploy"
  task :cold do
    update
    passenger::restart
  end

  desc "Restart Passenger"
  task :restart do
    passenger::restart
  end
  desc "reload the database with seed data"
  task :seed do
    run "cd #{current_path}; rake db:seed RAILS_ENV=#{rails_env}"
  end
end

namespace :passenger do
  desc "Restart Passenger"
  task :restart do
    run "cd #{current_path} && touch tmp/restart.txt"
  end
end
