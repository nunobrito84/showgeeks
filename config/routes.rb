Showgeeks::Application.routes.draw do

  # We need to define devise_for just omniauth_callbacks otherwise it does not work with scoped locales
  # see https://github.com/plataformatec/devise/issues/2813
  devise_for :users, skip: [:registration, :confirmation, :password, :session, :unlock], controllers: { :omniauth_callbacks => 'users/omniauth_callbacks' }

  scope '(:locale)' do
    # We define here a route inside the locale thats just saves the current locale in the session
    get 'omniauth/:provider' => 'users/omniauth#localized', as: :localized_omniauth

    devise_for :users, skip: :omniauth_callbacks,
               :controllers => {:registrations => 'users/devise/registrations',
                                :clients_confirmations => 'users/devise/confirmations',
                                :passwords => 'users/devise/passwords',
                                :sessions => 'users/devise/sessions',
                                :unlocks => 'users/devise/unlocks' }

    # NOTE: put this AFTER the 'devise_for :users' line
    resources :users, only: [:index, :show, :edit, :update, :destroy]

    resources :artists do
      get :run #To run a specific crawler from artist
    end
    resources :crawlers, only: [:index, :create] #this is the crawler detector to create the artist or venue, depending on the url

    get 'privacy' => 'application#privacy'
    get 'terms' => 'application#terms'
    match '/contacts',     to: 'contacts#new', via: 'get' #for the contact form
    resources 'contacts', only: [:new, :create]

    resources :shows do
      patch 'set_visible', :constraints => {:format => /(js)/}
    end
    match 'filters_state' => 'shows#filters_state', :via => :get, :constraints => {:format => /(json)/}

    resources :show_votes, only: [:index, :create, :destroy]
    resources :subcategory_votes, only: [:create, :index]
    resources :categories, only: :index

    namespace :states, :path => '/states/:state_id' do
      get '/cities/:id', :controller => 'maps', :action => 'get_cities'
    end

    # You can have the root of your site routed with "root"
    root 'shows#index'

    post 'reload_shows', :controller => 'shows', :action => 'reload_shows'
    post 'reload_suggestion', :controller => 'shows', :action => 'reload_suggestion'

    #There is a bug in the scoped routes with forms: https://github.com/rails/rails/issues/12178
    resources :countries, only: [:index, :show] do
      get 'districts_select'
      resources :districts, only: [:index, :show] do
        resources :cities do
          resources :venues do
            get :run #To run a specific crawler from website
          end
        end
      end
    end
  end

end
