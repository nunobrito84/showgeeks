class ICrawler

  # This is an interface module. If your class includes this module, make sure it responds to the following methods

  SEVERE_ERROR = 1   #This is a severe error we must look, such as malformatted items or when we cannot find a specific city
  LIGHT_ERROR = 2       #Such as when the date is in the past... it means we can ignore this error
  INFO = 3              #Such as when we have a trusted artist for this item

  YEAAAAH = 'pt.yeaaaah.com'
  FACEBOOK = 'www.facebook.com'
  BANDS_IN_TOWN = 'www.bandsintown.com'

  def initialize(obj)
    @success = 0
    @edit = 0
    @cancelled = 0
    @fail = 0
    @skip = 0
    @log = []
  end

  def set_duration(duration)
    @duration = duration
  end
  def duration
    @duration
  end
  def success
    @success
  end
  def edit
    @edit
  end
  def cancelled
    @cancelled
  end
  def fail
    @fail
  end
  def skip
    @skip
  end
  def log
    @log
  end

  def append_log_item(level, message)
    @log.push(level: level, message: message)
  end

  def url #Show specific url
    raise 'show url function not implemented'
  end
  def country_alpha2
    raise 'country_alpha2 function not implemented'
  end
  def district_alpha2
    raise 'district_alpha2 function not implemented'
  end
  def city
    raise 'city function not implemented'
  end
  def name
    raise 'venue function not implemented'
    #@venueObj[:name]
  end
  def trust_level
    raise 'trust_level function not implemented'
    #@venueObj.trusted? ? Show::TRUSTED_SOURCE_LEVEL : Show::NOT_TRUSTED_SOURCE_LEVEL
  end
  def website
    raise 'website function not implemented'
    #@artistObj[:website]
  end

  def crawl
    raise 'Parse function not implemented'
  end

  public
  def self.parse_user_id(crawler_url)
    resArray = URI(crawler_url).path.split('/')
    if(resArray.size > 1)
      resArray[1]
    elsif(resArray.size == 1)
      resArray[0]
    else
      nil
    end
  end

  public
  def self.parse_bands_in_town_id(crawler_url)
    resArray = URI(crawler_url).path.split('/')
    if(resArray.size > 1)
      URI.decode(resArray[1])
    elsif(resArray.size == 1)
      URI.decode(resArray[0])
    else
      nil
    end
  end

  public
  def self.get_crawler_class(url)
    require 'uri'
    URI.parse(url).host
  end

  def self.get_crawler_class_icon(url)
    require 'uri'
    case URI.parse(url).host
      when FACEBOOK
        ActionController::Base.helpers.image_tag('http://fbstatic-a.akamaihd.net/rsrc.php/yl/r/H3nktOa7ZMg.ico', size: '16x16')
      when BANDS_IN_TOWN
        ActionController::Base.helpers.image_tag('http://www.bandsintown.com/favicon.ico', size: '16x16')
      when YEAAAAH
        ActionController::Base.helpers.image_tag('http://pt.yeaaaah.com/img/icons/favicon.png', size: '16x16')
      else
        "<i class='glyphicon glyphicon-question-sign' height='16' width='16'></i>"
    end
  end

end