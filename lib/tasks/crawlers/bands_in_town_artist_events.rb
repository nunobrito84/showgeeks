include ActionView::Helpers::SanitizeHelper
class BandsInTownArtistEvents < IArtistCrawler

  def website
    artist_obj.website
  end
  def url #Show specific url
    nil
  end
  def title
    artist_obj.name
  end
  def trust_level
    artist_obj.trusted? ? Show::TRUSTED_SOURCE_LEVEL : Show::NOT_TRUSTED_SOURCE_LEVEL
  end
  def artist_id
    artist_obj.id
  end

  DELAY_SECONDS = 2.seconds
  MAX_NUMBER_OF_NEW_SHOWS = 1000

  def count_request_performed_sleep
    #Increase the request counter and perform delay
    @nShowsLimit += +1;
    if @nShowsLimit < MAX_NUMBER_OF_NEW_SHOWS
      sleep(DELAY_SECONDS) #avoid flooding
    end
  end

  def crawl
      @nShowsLimit = 0

      p "[Band In Town] Parsing artist #{title}..."
      today_date = Date.today
      admin_user = User.find_by(email: 'nunobrito84@gmail.com')
      raise 'The user was not found in the Showgeeks database' if admin_user.nil?

      events = Bandsintown::Artist.new({:name => ICrawler.parse_bands_in_town_id(artist_obj.crawler_feed_url)}).events
      events.each do |event|
        begin
          evt_name = artist_obj.name
          evt_description = ''
          evt_venue = event.venue.name

          evt_datetime = event.datetime
          evt_datetime_is_date_only = false

          country_list = Country.find_by_name(event.venue.country)
          raise "No country found: #{event.venue.country}" if country_list.nil?
          evt_country_alpha2 = country_list.first

          #we have this country enabled ?
          raise "Blocked country  #{evt_country_alpha2} - Skip event" if(evt_country_alpha2 != 'PT' && evt_country_alpha2 != 'US')

          evt_state = event.venue.region #get districtl
          raise "State parse error: #{event.venue.region}" if event.venue.region.nil?
          cityStr = event.venue.city #get districtl
          raise "City parse error: #{event.venue.region}" if event.venue.city.nil?

          #Lets find the city
          if evt_state and evt_country_alpha2 == 'US' #filter by district also
            cities = City.where(country_alpha2: evt_country_alpha2, district_alpha2: evt_state).where("name = ? OR name_alt = ?", cityStr, cityStr)
          else #just city name
            cities = City.where(country_alpha2: evt_country_alpha2).where("name = ? OR name_alt = ?", cityStr, cityStr)
          end

          raise "City not found: #{evt_country_alpha2} > #{cityStr}" if cities.empty?
          raise "Multiple cities found for: #{evt_country_alpha2} > #{cityStr}" if cities.any? and cities.size > 1
          city = cities.first

          #Lets find if there is any venue associated...
          venues = Venue.where(city_id: city.id).where("name LIKE ?", evt_venue)
          venue = venues.first

          #Lets find the if this show exists, perhaps due to other crawler sources
          if(venue)
            show = Show.where(name: evt_name, venue_id: venue.id, date: evt_datetime, city_id: city.id).first
          else
            show = Show.where(name: evt_name, date: evt_datetime, city_id: city.id).first
          end
          begin
            if(show.nil?) #Lets create the show
              show = Show.create!(
                  user: admin_user,
                  name: evt_name,

                  subcategory_id: artist_obj.subcategory_id,
                  category_id: artist_obj.subcategory.category_id,

                  date: evt_datetime,
                  is_date_only: evt_datetime_is_date_only,

                  country_alpha2: city.country_alpha2,
                  district_alpha2: city.district_alpha2,
                  city_id: city.id,
                  city_name: city.name,

                  artist_id: artist_obj.id,
                  artist_icon: artist_obj.icon_url,

                  venue_id: venue.try(:id),
                  venue_name: venue.nil? ? evt_venue : venue.name,
                  venue_website: venue.nil? ? nil : venue.website,
                  venue_icon: venue.nil? ? nil : venue.icon_url,

                  description: evt_description,
                  photo_url: artist_obj.photo_url,
                  trust: trust_level,
                  facebook_event_id: nil
              )
              SubcategoryVote.create!(user_id: show.user.id, show_id: show.id, subcategory_id: artist_obj.subcategory_id, amount: show.user.trust)
              @success += +1
            else #Update item
                 #When updating, we must give priority for the info on the venue_crawler, because they will be the paying ones and should care more about the information quality
              if(show.venue_id) #There is a venue associated to this event... give priority to the venue info
                if(!show.description.blank?)
                  evt_description = show.description #Keep the description because it can br from the venue crawler
                end
                if(!show.photo_url.blank?)
                  fb_event_picture = show.photo_url
                end
                if(show.trust.to_i > trust_level) #Use the higher one
                  edit_trust_level = show.trust
                end
                if(show.facebook_event_id != fb_event_id) #Keep the venue reference
                  fb_event_id = show.facebook_event_id
                end
              end
              if(show.description == evt_description   and
                  show.photo_url == fb_event_picture    and
                  show.trust == trust_level             and
                  show.facebook_event_id == fb_event_id and
                  show.subcategory_id == artist_obj.subcategory_id)
                @skip += +1
              else
                #If there is no category vote, do it!
                if show.subcategory_id.nil?
                  SubcategoryVote.create!(user_id: show.user.id, show_id: show.id, subcategory_id: artist_obj.subcategory_id, amount: show.user.trust)
                end
                show.update_attributes(
                    description: evt_description,
                    photo_url: fb_event_picture,
                    trust: edit_trust_level,
                    facebook_event_id: fb_event_id,

                    subcategory_id: artist_obj.subcategory_id,
                    category_id: artist_obj.subcategory.category_id
                )
                @edit += +1
              end
              puts "[#{evt_datetime.to_date}]: #{evt_name} @ #{evt_venue}"
            end
          rescue Exception => e
            append_log_item(SEVERE_ERROR, "Validation error for Id: #{fb_event_id} [#{evt_datetime.to_date}] #{evt_name} @ #{evt_venue};#{evt_city};#{evt_country_alpha2}: #{e.message}")
            @fail += + 1
          end
        rescue Exception => e
          append_log_item(SEVERE_ERROR, "Exception: #{event.bandsintown_id}: #{evt_name}: #{e.message}")
          @fail += + 1
        end
      end

      count_request_performed_sleep
      #parse(doc)
      p "Finished by reaching the limit number of shows... (#{MAX_NUMBER_OF_NEW_SHOWS})" if (@nShowsLimit >= MAX_NUMBER_OF_NEW_SHOWS)
  end

end
