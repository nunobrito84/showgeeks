require 'rss'
require 'open-uri'
include ActionView::Helpers::SanitizeHelper
class TheatroCirco < IVenueCrawler

  def title
    venue_obj.name
  end
  def website
    venue_obj.website
  end
  def url #Show specific url
    nil
  end
  def country_alpha2
    venue_obj.city.country_alpha2
  end
  def district_alpha2
    raise 'You cannot use this value from the venue on this crawler'
  end
  def city
    venue_obj.city.name
  end
  def venue_name
    venue_obj.name
  end
  def trust_level
    venue_obj.trusted? ? Show::TRUSTED_SOURCE_LEVEL : Show::NOT_TRUSTED_SOURCE_LEVEL
  end

  def crawl
    def parseItem(rssItem)
      titleNodeStr = rssItem.search('title')[0].text
      categoryStr = rssItem.search('category')[0].text

      descriptionXml = Nokogiri::HTML(rssItem.search('description')[0].text)
      imageStr = descriptionXml.xpath('//img').first.get_attribute('src')
      descriptionStr = descriptionXml.search('p').first.text

      linkStr = rssItem.search('link')[0].text

      artistStr = titleNodeStr.scan(/.*\(/).first[0..-2].to_s.strip.titleize
      dateStr = titleNodeStr.scan(/\([^\(]*$/).first[1..-2].to_s.strip

      #day = dateStr.scan(/\d{1,2}/).first
      month = 00
      digitsScan = dateStr.scan(/\d{1,2}/)
      dayStr = digitsScan.first
      hoursStr = digitsScan[1].to_s
      minutesStr = digitsScan[2].to_s
      year = Time.now.year

      ApplicationHelper::MONTHS_PT.each_with_index do |monthName, index|
        if dateStr.index(monthName.upcase[0..2]) != nil
          month = index+1
        end
      end
      showDate = Time.parse("#{year}/#{month}/#{dayStr} #{hoursStr}:#{minutesStr}:GMT")
      dateStr = showDate.strftime('%Y-%m-%d')
      timeStr = showDate.strftime('%T')
      #puts "IMG: #{imageStr}, DESC: #{descriptionStr}"

      puts "[#{dateStr} #{timeStr}]: #{artistStr} @ #{venue_name} (#{categoryStr})"

      city = City.find_by(country_alpha2: country_alpha2, name: self.city)

      show = Show.find_by(user: @admin_user,
                          name: artistStr,
                          venue_name: venue_name,
                          date: dateStr,
                          time: timeStr,
                          country_alpha2: country_alpha2,
                          district_alpha2: city.district_alpha2,
                          city_id: city.id)
      begin
        if(show == nil)
          show = Show.create!(
              user: @admin_user,
              name: artistStr,
              date: showDate,
              time: timeStr,
              country_alpha2: country_alpha2,
              district_alpha2: city.district_alpha2,
              city_id: city.id,
              venue_id: venue_obj.id,
              canceled: false,
              description: descriptionStr,
              photo_url: imageStr,
              url: linkStr,
              price: '',
              trust: trust_level  #This is trustable
          )
          @success += +1
        else #Update item
          show.update_attributes(
              description: descriptionStr,
              photo_url: imageStr,
              url: linkStr,
              website: website
          )
          @edit += +1
        end
        puts "[#{showDate}]: #{artistStr} @ #{venue_name}"
      rescue Exception => e
        append_log_item(SEVERE_ERROR, "Validation error for [#{showDate}] #{artistStr} @ #{venue_name};#{self.city};#{country_alpha2}: #{e.message}")
        @fail += + 1
      end
    end

    def parse(htmlContent)
      @admin_user = User.find_by(email: 'nunobrito84@gmail.com')
      nodes = htmlContent.search('item')
      nodes.each do |item|
        begin
          parseItem(item)
        rescue Exception => ex
          @fail += 1
          append_log_item(SEVERE_ERROR, "[#{ex.message}]\n#{ex.backtrace[0..5].join('\n')}")
        end
      end
    end

    if(Rails.env == 'production')
      doc = Nokogiri::XML(open(url))
    else
      doc = Nokogiri::XML(File.read('lib/tasks/crawlers/examples/theatro_circo_example.html'))
    end

    puts 'Parsing...'
    parse(doc)

  end
end
