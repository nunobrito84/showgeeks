require 'rss'
require 'open-uri'
require 'uri'

include ActionView::Helpers::SanitizeHelper
require_relative 'i_venue_crawler'

class Yeaaaah < IVenueCrawler

  def title
    'Yeaaaah.com'
  end
  def website
    nil #This must be nil, because it is not a trusted venue
  end
  def description
    ''
  end
  def url #Show specific url
    venue_obj.crawler_feed_url
  end
  def country_alpha2
    venue_obj.city.country_alpha2 #This crawler only deals with portuguese shows
  end
  def district_alpha2
    venue_obj.city.district_alpha2
  end
  def city_id
    venue_obj.city_id
  end
  def venue_name
    raise 'You cannot use this value from the venue on this crawler'
  end
  def venue_id
    venue_obj.id
  end
  def trust_level
    Show::NOT_TRUSTED_SOURCE_LEVEL
  end
  def category_id
    Category::Music
  end

  DELAY_SECONDS = 1.seconds
  MAX_NUMBER_OF_NEW_SHOWS = 100

  def crawl
    if(true or Rails.env == 'production')
      doc = Nokogiri::HTML(open(url))
    else
      p 'loading local file'
      doc = Nokogiri::HTML(File.read('lib/tasks/crawlers/examples/yeaaaah_example.html'))
    end
    puts 'Parsing...'

    @nShowsLimit = 0
    parse(doc)
    p "Finished by reaching the limit number of shows... (#{MAX_NUMBER_OF_NEW_SHOWS})" if (@nShowsLimit >= MAX_NUMBER_OF_NEW_SHOWS)
  end

  def parse(htmlContent)
    nodes = htmlContent.search("//div[@class='concerts_listing']//h3|//table") #Get h3 or table elements

    #Functions to parse date and table
    def parseHeaderDate(dateHtml)
      dateStr = dateHtml.text.to_s
      day = dateStr.scan(/\d{1,2}/).first
      month = 00
      year = dateStr.scan(/\d{4}/).first

      ApplicationHelper::MONTHS_PT.each_with_index do |monthName, index|
        if dateStr.index(monthName) != nil
          month = index+1
        end
      end
      date = Time.new(year,month,day).to_date
      return date
    end

    def getArtistName(listRow)
      artistNodes = listRow.search("td[@class='artists']/a")
      raise "No artist nodes found in table (search by: td[@class='artists']): #{listRow}" if artistNodes.empty?
      return artistNodes.first.text.strip.to_s[ 0..254]
    end

    def getShowUrl(listRow)
      hrefNodes = listRow.search("td[@class='artists']/a").map { |link| link['href'] }
      raise "No links for show (search by: td[@class='artists']): #{listRow}" if hrefNodes.empty?
      return "#{URI.parse(url).scheme}://#{URI.parse(url).host}#{hrefNodes.first}"
    end

    def getVenueName(listRow)
      venueNodes = listRow.search("td[@class='city_venue']")
      raise "No venue found in table (search by: td[_class='city_venue']): #{listRow}" if venueNodes.empty?
      venueArray = venueNodes.first.text.to_s.split('–')
      if venueArray.size < 1 or venueArray.size > 2
        raise "Can't split venue by '–': #{venueNodes.first}"
      end
      (venueArray.size == 2)?(venueArray[1].strip.to_s) : '-' #if there is no venue, allow a space
    end

    def getCityName(listRow)
      venueNodes = listRow.search("td[@class='city_venue']")
      raise "No venue found in table (search by: td[_class='city_venue']): #{listRow}" if venueNodes.empty?
      venueArray = venueNodes.first.text.to_s.split('–')
      if venueArray.size < 1 or venueArray.size > 2
        raise "Can't split venue by '–': #{venueNodes.first}"
      end
      venueArray[0].strip.to_s
    end

    def getCanceled(listRow)
      canceledNodes = listRow.search("td[@class='artists']/span[@class='cancelled_concert_mark']")
      canceledNodes.size > 0
    end

    def parseTable(tableHtml, showDate)
      entriesList = tableHtml.search("tr")
      raise "No rows found in table (search by: tr): #{tableHtml}" if entriesList.empty?
      raise "Showdate is nil: #{tableHtml}" if showDate.nil?
      admin_user = User.find_by(email: 'nunobrito84@gmail.com')
      raise "The user was not found in the Showgeeks database" if admin_user.nil?

      #go for each list line
      entriesList.each do |listRow|
        return if @nShowsLimit >= MAX_NUMBER_OF_NEW_SHOWS
        begin
          #get ARTIST
          artistStr = getArtistName(listRow)
          raise "No artist found in table (search by: td[@class='artists']): #{listRow}" if artistStr.nil?

          #get VENUE
          venueStr = getVenueName(listRow)
          raise "VenueName was not found (search by: td[_class='city_venue']): #{listRow}" if venueStr.nil?

          #get City
          cityStr = getCityName(listRow)
          raise "Error [City name not found: country: #{country_alpha2}. Skiping entry..." if cityStr.nil?
          city = City.find_by(country_alpha2: country_alpha2, name: cityStr)
          raise "Error [City not found: country: #{country_alpha2} | City: #{cityStr}]. Skiping entry..." if city.nil?

          #get canceled
          canceledShow = getCanceled(listRow)

          #we can now check which shows we already have to avoid a flood-attack on the yeaaah server
          show = Show.where("DATE(date) = ?", showDate).find_by(
              user: admin_user,
              name: artistStr,
              venue_id: venue_obj.id,
              venue_name: venueStr,
              city_id: city.id)

          if show #if we already have this show, skip
            if show.canceled != canceledShow #Update item, but only the cancel
              show.update_attributes( canceled: canceledShow )
              @cancelled += +1
            else
              p "Skip #{artistStr} : we already had this show"
              @skip += +1
            end
            next
          end

          # *** NEW SHOW ***   ... lets get the date and create the show

          #Get the DATE
          show_is_date_only = false
          showDateTime = nil
          showUrl = getShowUrl(listRow)

          #Increase the request counter and perform delay
          @nShowsLimit += +1;
          if @nShowsLimit < MAX_NUMBER_OF_NEW_SHOWS
            sleep(DELAY_SECONDS) #avoid flooding
          end

          showDoc = Nokogiri::HTML(open(showUrl))
          if showDoc.search('time').any?
            datetimeNode = showDoc.search('time').first['datetime']
            show_is_date_only = !datetimeNode.include?(':')
            showDateTime = DateTime.parse "#{datetimeNode}+0100"
          end
          raise "Can't find DateTime for '–': #{artistStr}" if showDateTime.nil?
          if(showDateTime.to_date < DateTime.now.to_date)
            p "Too old -> Skip #{showDate}] #{artistStr} @ #{venueStr};#{cityStr};#{country_alpha2}"
            next # dont report, it is not necessary
          end

          begin
            show = Show.create!(
                user: admin_user,
                name: artistStr,
                visible: Show::VISIBLE_DONT_KNOW,
                date: showDateTime,
                is_date_only: show_is_date_only,

                category_id: category_id,

                country_alpha2: city.country_alpha2,
                district_alpha2: city.district_alpha2,
                city_id: city.id,
                city_name: city.name,

                venue_id: venue_obj.id,
                venue_name: venueStr,

                canceled: canceledShow,
                trust: trust_level,
                description: description
            )
            @success += +1
          rescue Exception => e
            append_log_item(LIGHT_ERROR, "Validation error for [#{showDate}] #{artistStr} @ #{venueStr};#{cityStr};#{country_alpha2}: #{e.message}")
            @fail += + 1
            next
          end

        rescue Exception => e #light exception... only the table row was affected... go to next
          append_log_item(SEVERE_ERROR, e.message)
          @fail += +1
          next
        end
        p "##{@nShowsLimit}  #{'{cancelled} ' if canceledShow }[#{showDate}]: #{artistStr} @ #{venueStr}"
      end
    end

    #Get both date and table items
    showDate = nil
    p "Nodes detected: #{nodes.size}"
    nodes.each do |item|
      case item.name
        when 'h3'
          showDate = parseHeaderDate(item)
        when 'table'
          parseTable(item, showDate)
      end
    end
  end
end
