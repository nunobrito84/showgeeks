class IArtistCrawler < ICrawler

  RECOGNIZED_CRAWLERS = [ FACEBOOK, BANDS_IN_TOWN ]

  def initialize(artistObj)
    super
    @artistObj = artistObj

    #Lets get the other artists where the sources are trusted
    #@trusted_venues = Venue.all
    #puts "Trusted venues: #{@trusted_venues.inspect}"

  end

  def artist_obj
    @artistObj
  end

end