require 'rss'
require 'open-uri'
require_relative 'i_crawler'
include ActionView::Helpers::SanitizeHelper

require "#{Rails.root}/app/helpers/crawlers_helper"
include CrawlersHelper

namespace :crawlers do

  task :run => :environment do

    p "Environment: #{Rails.env}"
    venueJobList = artistJobList = []

    #if(Rails.env.production?)
      artistJobList = crawlArtists
    #end
    if(Rails.env.production?)
      venueJobList = crawlVenues
    end

    if(Rails.env.production? or Rails.env.development?)
      ShowCrawlerNotifier.processed(artistJobList, venueJobList).deliver
    end

  end

  def crawlYeaaaah
    return []
    yeahhhhJob = Yeaaaah.new
    puts "** Starting Yeaaaah Crawler Job..."
    puts "----------------------------------------------------------------------------"
    artistJob.crawl
    puts "----------------------------------------------------------------------------"
    puts "--- Yeaaaah Finished #{artistJob.title} ... success=#{artistJob.success}, edit=#{artistJob.edit}, cancelled=#{artistJob.cancelled}, fail=#{artistJob.fail}, skip=#{artistJob.skip}, LOG=#{artistJob.log.to_sentence}"
    puts "----------------------------------------------------------------------------"
    logStr = crawlerLogAsHtmlList(artistJob.log)
    ArtistCrawlerReport.create(artist_id: artistJob.artist_id, success_count: artistJob.success, edit_count: artistJob.edit, cancelled_count: artistJob.cancelled, fail_count: artistJob.fail, skip_count: artistJob.skip, log: logStr, automatic: true)
    puts "** Finished Artist Crawler Job..."
    return artistJobList
  end

  def crawlArtists
    puts "** Starting Artists Crawler Jobs..."
    artistJobList = []
    artistList =  Artist.where(enable_crawler: true)
    #if(Rails.env.development?)
    #  artistList = artistList.where(id: 5)
    #end
    artistList.each do |artist|
    #Artist.where(enable_crawler: true).each do |artist|
      case ICrawler.get_crawler_class(artist.crawler_feed_url)
        when IVenueCrawler::FACEBOOK
          artistJobList.push FacebookArtistEvents.new(artist)
        when IVenueCrawler::BANDS_IN_TOWN
          artistJobList.push BandsInTownArtistEvents.new(artist)
        else
          logStr = "Unknown artist crawler name for artist: #{artist.name}"
        #use default crawler
        #crawler.last_log = "Unknown crawler name: #{@crawler.name}"
      end
    end
    #Check the interface
    artistJobList.each do |artistJob|
      if !artistJob.is_a? ICrawler
        raise 'This class MUST be ICrawler inherited!!'
      end
    end
    artistJobList.each do |artistJob|
      initial_time = Time.now
      exceptionRaised = 0
      begin
        puts "----------------------------------------------------------------------------"
        puts "--- Artist Starting #{artistJob.title}"
        puts "----------------------------------------------------------------------------"
        artistJob.crawl
        puts "----------------------------------------------------------------------------"
        puts "--- Artist Finished #{artistJob.title} ... duration=#{Time.at(Time.now.to_i - initial_time.to_i).utc.strftime("%H:%M:%S")} success=#{artistJob.success}, edit=#{artistJob.edit}, cancelled=#{artistJob.cancelled}, fail=#{artistJob.fail}, skip=#{artistJob.skip}, LOG=#{artistJob.log.to_sentence}"
        puts "----------------------------------------------------------------------------"
        logStr = crawlerLogAsHtmlList(artistJob.log)
      rescue Exception => e
        p e.backtrace
        exceptionRaised = 10000 #This is to raise a need to look warning
        logStr = "Artist exception raised: #{e.message}\nBacktrace:\n#{e.backtrace[0..5].join('\n')}"
      end
      artistJob.set_duration(Time.at(Time.now.to_i - initial_time.to_i).utc)
      ArtistCrawlerReport.create(artist_id: artistJob.artist_id, duration: artistJob.duration, success_count: artistJob.success, edit_count: artistJob.edit, cancelled_count: artistJob.cancelled, fail_count: artistJob.fail + exceptionRaised, skip_count: artistJob.skip, log: logStr, automatic: true)
    end
    puts "** Finished Artist Crawler Job..."
    return artistJobList
  end

  def crawlVenues
    puts "** Starting Venue Crawler Jobs..."
    venueJobList = []
    Venue.where(enable_crawler: true).each do |venue|
    #Venue.where(enable_crawler: true, id: 1).each do |venue|
      case ICrawler.get_crawler_class(venue.crawler_feed_url)
        when IVenueCrawler::FACEBOOK
          venueJobList.push FacebookVenueEvents.new(venue)
        when IVenueCrawler::YEAAAAH
          venueJobList.push Yeaaaah.new(venue)
        else
          logStr = "Unknown venue crawler name for venue: #{venue.name}"
        #use default crawler
        #crawler.last_log = "Unknown crawler name: #{@crawler.name}"
      end
    end
    #Check the interface
    venueJobList.each do |venueJob|
      if !venueJob.is_a? ICrawler
        raise 'This class MUST be ICrawler inherited!!'
      end
    end
    venueJobList.each do |venueJob|
      initial_time = Time.now
      exceptionRaised = 0
      begin
        puts "----------------------------------------------------------------------------"
        puts "--- Venue Starting #{venueJob.title}"
        puts "----------------------------------------------------------------------------"
        venueJob.crawl
        puts "----------------------------------------------------------------------------"
        puts "--- Venue Finished #{venueJob.title} ... duration=#{Time.at(Time.now.to_i - initial_time.to_i).utc.strftime("%H:%M:%S")} success=#{venueJob.success}, edit=#{venueJob.edit}, cancelled=#{venueJob.cancelled}, fail=#{venueJob.fail}, skip=#{venueJob.skip}, LOG=#{venueJob.log.to_sentence}"
        puts "----------------------------------------------------------------------------"
        logStr = crawlerLogAsHtmlList(venueJob.log)
      rescue Exception => e
        p e.backtrace
        exceptionRaised = 10000 #This is to raise a need to look warning
        logStr = "Venue exception raised: #{e.message}\nBacktrace:\n#{e.backtrace[0..5].join('\n')}"
      end
      venueJob.set_duration(Time.at(Time.now.to_i - initial_time.to_i).utc)
      VenueCrawlerReport.create(venue_id: venueJob.venue_id, duration: venueJob.duration, success_count: venueJob.success, edit_count: venueJob.edit, cancelled_count: venueJob.cancelled, fail_count: venueJob.fail + exceptionRaised, skip_count: venueJob.skip, log: logStr, automatic: true)
    end
    puts "** Finished Venue Crawler Job..."
    return venueJobList
  end

end
