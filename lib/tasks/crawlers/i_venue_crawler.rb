class IVenueCrawler < ICrawler

  RECOGNIZED_CRAWLERS = [ YEAAAAH, FACEBOOK ]#, BANDS_IN_TOWN ]

  def initialize(venueObj)
    super
    @venueObj = venueObj

    #Lets get the other venues where the sources are trusted
    #@other_trusted_venues = Venue.where("id != ? AND trusted = true", venueObj[:id])
    #puts "Other trusted venues: #{@other_trusted_venues.inspect}"

  end

  def venue_obj
    @venueObj
  end

end