include ActionView::Helpers::SanitizeHelper
class FacebookVenueEvents < IVenueCrawler

  def title
    venue_obj.name
  end
  def website
    venue_obj.website
  end
  def url #Show specific url
    nil
  end
  def country_alpha2
    venue_obj.city.country_alpha2 #This crawler only deals with portuguese shows
  end
  def district_alpha2
    venue_obj.city.district_alpha2
  end
  def city_id
    venue_obj.city.id
  end
  def city
    venue_obj.city.name
  end
  def venue_id
    venue_obj.id
  end
  def venue_name
    venue_obj.name
  end
  def trust_level
    venue_obj.trusted? ? Show::TRUSTED_SOURCE_LEVEL : Show::NOT_TRUSTED_SOURCE_LEVEL
  end

  DELAY_SECONDS = 3.seconds
  MAX_NUMBER_OF_NEW_SHOWS = 1000

  def count_request_performed_sleep
    #Increase the request counter and perform delay
    @nShowsLimit += +1;
    if @nShowsLimit < MAX_NUMBER_OF_NEW_SHOWS
      sleep(DELAY_SECONDS) #avoid flooding
    end
  end

  def crawl
      @nShowsLimit = 0

      p "[Facebook] Parsing venue #{title}..."
      today_date = Date.today
      @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
      admin_user = User.find_by(email: 'nunobrito84@gmail.com')
      raise "The user was not found in the Showgeeks database" if admin_user.nil?

      events = @graph.get_connections(ICrawler.parse_user_id(venue_obj.crawler_feed_url), 'events', {since: today_date})
      count_request_performed_sleep

      p "Captured #{events.size} events "
      events.each do |event|
        p "--> #{event['name']}"
      end

      while(events and @nShowsLimit < MAX_NUMBER_OF_NEW_SHOWS)
        events.each do |event|
          break if @nShowsLimit >= MAX_NUMBER_OF_NEW_SHOWS
          begin
            evt_name = event['name'].mb_chars.titleize
            fb_event_id = event['id']

            #lets see if we have this show
            show = Show.find_by(facebook_event_id: fb_event_id)
            if show #if we already have this show, skip
              p "Skip #{evt_name} : we already had this show"
              @skip += +1
              next # Go to next event
            end

            #p event_info.inspect
            evt_datetime = DateTime.parse(event['start_time'])
            if(evt_datetime < DateTime.now)
              append_log_item(INFO, "Skip [#{evt_datetime.to_date}] #{evt_name} @ #{venue_name};#{city};#{country_alpha2} because the date is in the past.")
              @fail += +1
              next #Skip... go to next     l
            end

            event_info = @graph.get_connections(fb_event_id, '')
            fb_pic = @graph.get_connections(fb_event_id, 'picture?redirect=false', {height: '200', width: '200'})
            count_request_performed_sleep

            fb_event_picture = fb_pic['data']['url'] if !fb_pic['data']['is_silhouette']

            show = Show.find_by(name: evt_name, venue_id: venue_obj.id, date: evt_datetime,
                                country_alpha2: country_alpha2, district_alpha2: district_alpha2, city_id: venue_obj.city.id)

            artistStr = evt_name
            p "Artist evt: #{artistStr}"
            descriptionStr = event_info['description'][0..1023]
            evt_datetime_is_date_only = event_info['is_date_only']

            begin
              if(show == nil)
                #Lets see if we have such artist
                artist = Artist.find_by(name: artistStr)

                show = Show.create!(
                    user: admin_user,
                    name: artistStr,

                    subcategory_id: artist.try(:subcategory_id),
                    category_id: artist.try(:subcategory).try(:category_id),

                    date: evt_datetime,
                    is_date_only: evt_datetime_is_date_only,

                    country_alpha2: country_alpha2,
                    district_alpha2: district_alpha2,
                    city_id: venue_obj.city.id,
                    city_name: venue_obj.city.name,

                    venue_id: venue_obj.id,
                    venue_name: venue_obj.name,
                    venue_website: venue_obj.website,
                    venue_icon: venue_obj.icon_url,

                    artist: artist,
                    artist_icon: artist.try(:icon_url),

                    description: descriptionStr,
                    photo_url: fb_event_picture,
                    trust: trust_level,
                    facebook_event_id: fb_event_id
                )
                @success += +1
              else #Update item
                #When updating, we must give priority for the info on the venue_crawler, because they will be the paying ones and care more about the information quality
                #Give priority to the info from the venue info
                if(descriptionStr.blank?) #If venue has no description, use the show description from the artist crawler
                  descriptionStr = show.description #Keep the description because it can be from the artist crawler
                end
                if(fb_event_picture.blank?)
                  fb_event_picture = show.photo_url
                end
                evt_trust_level = trust_level
                if(evt_trust_level < show.trust) #Use the higher one
                  evt_trust_level = show.trust
                end
                #Keep the facebook venue reference
                if(show.description == descriptionStr and
                    show.photo_url == fb_event_picture and
                    show.trust == evt_trust_level and
                    show.facebook_event_id == fb_event_id)
                  @skip += +1
                else
                  show.update_attributes(
                      description: descriptionStr,
                      photo_url: fb_event_picture,
                      trust: evt_trust_level,
                      facebook_event_id: fb_event_id
                  )
                  @edit += +1
                end
              end
              puts "[#{evt_datetime.to_date}]: #{artistStr} @ #{venue_name}"
            rescue Exception => e
              append_log_item(SEVERE_ERROR, "Validation error for #{fb_event_id}: #{evt_datetime.to_date} #{artistStr} @ #{venue_name};#{city};#{country_alpha2}: #{e.message}")
              @fail += + 1
            end
          rescue Exception => e
            append_log_item(SEVERE_ERROR, "Exception: #{fb_event_id}: #{evt_name}: #{e.message}")
            @fail += + 1
          end
        end
        #break
        events = events.next_page
        #count_request_performed_sleep
      end

      #parse(doc)

    end

end
