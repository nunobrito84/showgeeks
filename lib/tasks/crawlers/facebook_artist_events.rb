include ActionView::Helpers::SanitizeHelper
class FacebookArtistEvents < IArtistCrawler

  def website
    artist_obj.website
  end
  def url #Show specific url
    nil
  end
  def title
    artist_obj.name
  end
  def trust_level
    artist_obj.trusted? ? Show::TRUSTED_SOURCE_LEVEL : Show::NOT_TRUSTED_SOURCE_LEVEL
  end
  def artist_id
    artist_obj.id
  end

  DELAY_SECONDS = 3.seconds
  MAX_NUMBER_OF_NEW_SHOWS = 1000

  def count_request_performed_sleep
    #Increase the request counter and perform delay
    @nShowsLimit += +1;
    if @nShowsLimit < MAX_NUMBER_OF_NEW_SHOWS
      sleep(DELAY_SECONDS) #avoid flooding
    end
  end

  def crawl
      @nShowsLimit = 0

      p "[Facebook] Parsing artist #{title}..."
      today_date = Date.today
      @graph = Koala::Facebook::API.new(Showgeeks::FACEBOOK_APPLICATION_TOKEN)
      admin_user = User.find_by(email: 'nunobrito84@gmail.com')
      raise "The user was not found in the Showgeeks database" if admin_user.nil?

      events = @graph.get_connections(ICrawler.parse_user_id(artist_obj.crawler_feed_url), 'events', {since: today_date})
      count_request_performed_sleep

      p "Captured #{events.size} events "
      events.each do |event|
        p "--> #{event['name']}"
      end

      #Lets crawl the events
      while(events and @nShowsLimit < MAX_NUMBER_OF_NEW_SHOWS)
        events.each do |event|
          break if @nShowsLimit >= MAX_NUMBER_OF_NEW_SHOWS
          begin
            evt_name = event['name']
            fb_event_id = event['id']
            p "processing #{fb_event_id}: #{evt_name}"

            #lets see if we have this show
            show = Show.find_by(facebook_event_id: fb_event_id)
            if show #if we already have this show, skip
              p "Skip #{evt_name} : we already had this show"
              @skip += +1
              next # Go to next event
            end

            #we don't have this show... lets get the info to create the show
            event_info = @graph.get_connections(fb_event_id, '')
            count_request_performed_sleep

            #p event_info.inspect
            evt_datetime = DateTime.parse(event_info['start_time'])
            evt_datetime_is_date_only = event_info['is_date_only']

            evt_name = artist_obj.name
            evt_description = ''#event_info['description'][0..1023]
            evt_venue = event_info['location']

            raise 'No venue location in this event...' if evt_venue.nil?
            raise 'No venue in this event...' if event_info['venue'].nil?

            evt_city = event_info['venue']['city'] || event_info['venue']['name']
            evt_state = event_info['venue']['state']

            evt_country = event_info['venue']['country']

            #Lets find the country
            evt_country_alpha2 = Country.find_country_by_name(evt_country).try(:alpha2) || artist_obj.country_alpha2
            raise "Country not found: #{evt_country}" if evt_country_alpha2.nil?

            #we have this country enabled ?
            raise "Blocked country  #{evt_country_alpha2} - Skip event" if(evt_country_alpha2 != 'PT' && evt_country_alpha2 != 'US')

            #Lets find the city
            if evt_state and evt_country_alpha2 == 'US' #filter by district also
              cities = City.where(country_alpha2: evt_country_alpha2, district_alpha2: evt_state).where("name = ? OR name_alt = ?", evt_city, evt_city)
            else #just city name
              cities = City.where(country_alpha2: evt_country_alpha2).where("name = ? OR name_alt = ?", evt_city, evt_city)
            end
            raise "City not found: #{evt_country} > #{evt_city}" if cities.empty?
            raise "Multiple cities found for: #{evt_country} > #{evt_city}" if cities.any? and cities.size > 1
            city = cities.first

            #Lets find if there is any venue associated...
            p "Event venue: #{evt_venue}"
            p "City id: #{city.id}"
            venue_without_cityname = evt_venue#.split(' ').delete_if {|name| name.include? city.name or name.include? city.name_alt }.join(' ')
            venues = Venue.where(city_id: city.id).where("name LIKE ?", venue_without_cityname)
            venue = venues.first

            #Get the event picture
            fb_pic = @graph.get_connections(fb_event_id, 'picture?redirect=false', {height: '200', width: '200'})
            count_request_performed_sleep

            fb_event_picture = fb_pic['data']['url'] if !fb_pic['data']['is_silhouette']

            #Lets find the if this show exists, perhaps due to other crawler sources
            if(venue)
              show = Show.where(name: evt_name, venue_id: venue.id, date: evt_datetime, city_id: city.id).first
            else
              show = Show.where(name: evt_name, date: evt_datetime, city_id: city.id).first
            end
            begin
              if(show.nil?) #Lets create the show
                show = Show.create!(
                    user: admin_user,
                    name: evt_name,

                    subcategory_id: artist_obj.subcategory_id,
                    category_id: artist_obj.subcategory.category_id,

                    date: evt_datetime,
                    is_date_only: evt_datetime_is_date_only,

                    country_alpha2: city.country_alpha2,
                    district_alpha2: city.district_alpha2,
                    city_id: city.id,
                    city_name: city.name,

                    artist_id: artist_obj.id,
                    artist_icon: artist_obj.icon_url,

                    venue_id: venue.try(:id),
                    venue_name: venue.nil? ? evt_venue : venue.name,
                    venue_website: venue.nil? ? nil : venue.website,
                    venue_icon: venue.nil? ? nil : venue.icon_url,

                    description: evt_description,
                    photo_url: fb_event_picture,
                    trust: trust_level,
                    facebook_event_id: fb_event_id)
                SubcategoryVote.create!(user_id: show.user.id, show_id: show.id, subcategory_id: artist_obj.subcategory_id, amount: show.user.trust)
                @success += +1
              else #Update item
                #When updating, we must give priority for the info on the venue_crawler, because they will be the paying ones and should care more about the information quality
                if(show.venue_id) #There is a venue associated to this event... give priority to the venue info
                  if(!show.description.blank?)
                    evt_description = show.description #Keep the description because it can br from the venue crawler
                  end
                  if(!show.photo_url.blank?)
                    fb_event_picture = show.photo_url
                  end
                  if(show.trust.to_i > trust_level) #Use the higher one
                    edit_trust_level = show.trust
                  end
                  if(show.facebook_event_id != fb_event_id) #Keep the venue reference
                    fb_event_id = show.facebook_event_id
                  end
                end
                if(show.description == evt_description   and
                   show.photo_url == fb_event_picture    and
                   show.trust == trust_level             and
                   show.facebook_event_id == fb_event_id and
                   show.subcategory_id == artist_obj.subcategory_id
                  )
                  @skip += +1
                else
                  #If there is no category vote, do it!
                  if show.subcategory_id.nil?
                    SubcategoryVote.create!(user_id: show.user.id, show_id: show.id, subcategory_id: artist_obj.subcategory_id, amount: show.user.trust)
                  end
                  show.update_attributes(
                      description: evt_description,
                      photo_url: fb_event_picture,
                      trust: edit_trust_level,
                      facebook_event_id: fb_event_id,

                      subcategory_id: artist_obj.subcategory_id,
                      category_id: artist_obj.subcategory.category_id
                  )

                  @edit += +1
                end
                puts "[#{evt_datetime.to_date}]: #{evt_name} @ #{evt_venue}"
              end
              rescue Exception => e
                append_log_item(SEVERE_ERROR, "Validation error for Id: #{fb_event_id} [#{evt_datetime.to_date}] #{evt_name} @ #{evt_venue};#{evt_city};#{evt_country_alpha2}: #{e.message}")
                @fail += + 1
              end
          rescue Exception => e
            append_log_item(SEVERE_ERROR, "Exception: #{fb_event_id}: #{evt_name}: #{e.message}")
            @fail += + 1
          end
          #p show.errors.inspect
          #p event.inspect
          #break
        end
        #break
        events = events.next_page
        #count_request_performed_sleep
      end

      #parse(doc)
      p "Finished by reaching the limit number of shows... (#{MAX_NUMBER_OF_NEW_SHOWS})" if (@nShowsLimit >= MAX_NUMBER_OF_NEW_SHOWS)
  end

end
